import React, {useRef, useState} from "react";
import useEventListener from "@use-it/event-listener";
import {createStyles, makeStyles} from "@material-ui/core/styles";
import classNames from "classnames";


const BUTTON_SIZE = 30;
const useStyles = makeStyles(theme =>
    createStyles({
        removeButtonContainer: {
            display: "block",
            outline: 0,
            backgroundColor: theme.palette.grey.A700,
            position: "absolute",
            top: 0,
            right: 0,
            overflow: "hidden",
            borderRadius: BUTTON_SIZE,
            // border: `1px ${theme.palette.grey.A400} solid`,
            boxShadow: "inset 1px 2px 3px -1px rgb(0 0 0 / 30%)",
            transition: ".4s",
            height: BUTTON_SIZE - 2,
            width: BUTTON_SIZE - 2,
            backgroundClip: "content-box",
            padding: 1,
            "&:focus": {
                outline: "none",
                width: "105px",
                "& > $removeButton.$focused": {
                    display: "inline-block",
                    backgroundColor: theme.palette.error.main,
                    fontColor: theme.palette.common.white,
                }
            },
            "& > $removeButton.$focused": {
                display: "none"
            }
        },
        removeButton: {
            position: "absolute",
            borderRadius: BUTTON_SIZE,
            fontColor: theme.palette.error.main,
            display: "inline-block",
            fontSize: theme.typography.caption.fontSize,
            margin: 0,
            top: 0,
            left: 0,
            backgroundColor: theme.palette.grey.A400,
            height: BUTTON_SIZE,
            width: BUTTON_SIZE,
            "&:hover": {
                fontColor: theme.palette.error.dark,
                cursor: "pointer"
            },
            "& > *": {
                display: "block",
                width: "100%",
                textAlign: "center",
                position: "absolute",
                top: "50%",
                transform: "translateY(-50%)"
            }
        },
        removeText: {
            fontSize: theme.typography.caption.fontSize,
            fontColor: theme.typography.caption.fontColor,
            opacity: "50%",
            whiteSpace: "nowrap",
            top: 8,
            left: BUTTON_SIZE + 8,
            position: "absolute",
        },
        dropZone: {
            boxShadow: "inset 2px 1px 2px -1px rgba(0,0,0,0.3)",
            position: "absolute",
            height: BUTTON_SIZE,
            width: BUTTON_SIZE,
            left: "77px",
            top: 0,
            backgroundColor: theme.palette.grey.A400,
            borderRadius: BUTTON_SIZE
        },
        hidden: {
            display: "none"
        },
        focused: {}
    })
);

export const ActionButton = (props: RemoveButtonProps) => {
    const classes = useStyles();
    let [dragging, setDragging] = useState(false);
    let [complete, setComplete] = useState(false);
    let [currentX, setCurrentX] = useState(0);
    const [focused, setFocused] = useState(false);
    let ref = useRef<HTMLDivElement>();

    let MIN = 0;
    let MAX = 75;
    useEventListener("mousemove", (evt: MouseEvent) => {
        if (dragging && !complete) {

            let x = ref.current.getBoundingClientRect().x;
            let currentOffset = evt.pageX - x - 15;
            let diff = Math.max(MIN, Math.min(currentOffset, MAX));

            setCurrentX(diff);

            if (currentOffset >= MAX) {
                setComplete(true);
                props.onComplete();
                ref.current.blur();
                setCurrentX(0);
            }
        }
    }, window);

    useEventListener("mouseup", (evt: MouseEvent) => {
        setDragging(false);
        setCurrentX(0);
    }, window);

    return <div
        className={classes.removeButtonContainer}
        draggable={false}
        tabIndex={0}
        onFocus={() => {
            setComplete(false);
            setFocused(true);
        }}
        onBlur={() => setFocused(false)}
        ref={ref}>
        <div className={classes.dropZone} onClick={() => ref.current.blur()}/>
        <span className={classes.removeText}>{props.label}</span>
        <span
            className={classNames(classes.removeButton, {[classes.hidden]: focused})}
            style={{
                left: `${complete ? "initial" : "0px"}`,
                right: `${complete ? "0px" : "initial"}`
            }}
        >
            {props.icon}
        </span>
        <span
            className={classNames(classes.removeButton, classes.focused, {[classes.hidden]: !focused})}
            style={{
                left: `${dragging ? currentX : 0}px`,
                // transform: `rotate(${currentX * 2.4}deg)`
            }}
            onMouseDown={() => setDragging(true)}
        >
            {props.icon}
        </span>
    </div>;
}

interface RemoveButtonProps {
    onComplete: () => void
    label: string,
    icon: React.ReactFragment
}