import React from "react";
import {IManagedPlayers} from "../hooks/useManagedPlayers";

export const ManagedPlayerContext = React.createContext<IManagedPlayers>({
    players: []
});