import {useEffect, useState} from "react";
import {getStompClient} from "../util/stomp";
import {Message} from "stompjs";
import {ManagedPlayerDTO} from "../importedTypes";

export interface IManagedPlayers {
    players: ManagedPlayerDTO[]
}

export function useManagedPlayers(): IManagedPlayers {
    const [players, setPlayers] = useState<ManagedPlayerDTO[]>([]);

    useEffect(() => {
        getStompClient().then(stompClient => {
            stompClient.subscribe("/topic/managedPlayers", (message: Message) => {
                setPlayers(JSON.parse(message.body));
            });
            setInterval(() => {
                getStompClient().then(stompClient => stompClient.send("/app/managedPlayers"))
            }, 1000);
        });
    }, [])

    return {
        players
    };
}