import {useEffect, useState} from "react";
import {deletePlayer, getAllGroups, getAllPlayers, savePlayer} from "../service/registry-service";
import {GroupDefinition, PlayerDefinition} from "../importedTypes";

export interface IRegistry {
    subjects: string[],
    players: PlayerDefinition[],
    groups: GroupDefinition[],
    unregisterPlayer: (username: string) => void,
    save: (username: string, active: boolean, password?: string) => void;
}

export function useRegistry(): IRegistry {
    const [subjects, setSubjects] = useState<string[]>([]);
    const [groups, setGroups] = useState<GroupDefinition[]>([]);
    const [players, setPlayers] = useState<PlayerDefinition[]>([]);

    useEffect(() => {
        invalidate();
    }, []);

    useEffect(() => {
        const updatedSubjects: string[] = [
            ...groups.map(group => group.name),
            ...players.filter(player => player.active).map(player => player.username)
        ];
        setSubjects(updatedSubjects);
    }, [groups, players])

    const invalidate = () => {
        getAllGroups().then(setGroups);
        getAllPlayers().then(setPlayers);
    }

    const unregisterPlayer = (username: string) => {
        console.log(`Unregistering player ${username}`)
        deletePlayer(username).then(invalidate);
    }

    const save = (username: string, active: boolean, password?: string) => {
        console.log(`Setting ${username} to active status ${active}`)
        savePlayer({
            username,
            active,
            password
        }).then(invalidate);
    }

    return {
        subjects,
        unregisterPlayer,
        players,
        groups,
        save
    }
}