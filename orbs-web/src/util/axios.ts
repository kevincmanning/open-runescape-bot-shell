import axiosClient from "axios";

export const axios = axiosClient.create({
    baseURL: "http://localhost:8080/",
});