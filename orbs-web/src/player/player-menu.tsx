import React, {useState} from "react";
import {Badge, Divider, Fab, IconButton, LinearProgress, makeStyles, Tooltip, Typography} from "@material-ui/core";
import {ScriptParamsModal} from "../script/script-params-modal";
import {loadScript, pauseScript, startScript, stopScript} from "../service/player-service";
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import {StatsPopover} from "../stats/stats-popover";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import QueueIcon from "@material-ui/icons/Queue";
import ViewListIcon from "@material-ui/icons/ViewList";
import MoreHorizOutlinedIcon from '@material-ui/icons/MoreHorizOutlined';
import {ManagedPlayerDTO, ScriptState} from "../importedTypes";

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(0.5),
        },
        'text-align': "center"
    },
    progressIndicator: {
        display: "flex",
        justifyContent: "space-between",
        padding: 10
    },
    progressIndicatorLine: {
        width: "65%",
        margin: "6px 10px",
        height: 2,
    }
}));

function formatScriptState(scriptState: ScriptState) {
    if (scriptState === null) {
        return "Idle";
    }
    const firstLetter = scriptState.slice(0, 1);
    const remaining = scriptState.slice(1);

    return firstLetter.toUpperCase() + remaining.replaceAll("_", " ").toLowerCase();
}

export function PlayerMenu({player}: { player: ManagedPlayerDTO }) {
    const styles = useStyles();
    const [paramsModalOpen, setParamsModalOpen] = useState(false);
    const [viewStatsModalOpen, setViewStatsModalOpen] = useState(false);
    const [viewStatsEl, setViewStatsEl] = useState<null | HTMLElement>(null);
    const {username, scriptElapsedTime, scriptState} = player;

    return <div className={styles.root}>
        <Divider/>
        <Tooltip title={"View Stats"}>
            <IconButton
                ref={(ref) => setViewStatsEl(ref)}
                onClick={() => setViewStatsModalOpen(true)}>
                <MoreHorizOutlinedIcon/>
            </IconButton>
        </Tooltip>
        <Tooltip title={"View Queue"}>
            <IconButton
                disabled={true}
                onClick={() => {
                }}>
                <Badge variant={"dot"} color={"primary"}>
                    <ViewListIcon/>
                </Badge>
            </IconButton>
        </Tooltip>
        {
            scriptState === ScriptState.RUNNING &&
            <Tooltip title={"Pause Script"}>
                <Fab color="primary" size={"small"} onClick={() => pauseScript(username)}>
                    <PauseIcon fontSize={"small"}/>
                </Fab>
            </Tooltip>
        }
        {
            scriptState !== ScriptState.RUNNING &&
            <Tooltip title={"Play Script"}>
                <Fab
                    color="primary"
                    size={"small"}
                    onClick={() => startScript(username)}
                    disabled={scriptState === ScriptState.SLEEPING || scriptState == null}
                >
                    <PlayArrowIcon fontSize={"small"}/>
                </Fab>
            </Tooltip>
        }
        <Tooltip title={"Next Script"}>
            <IconButton
                onClick={() => stopScript(username)}>
                <SkipNextIcon/>
            </IconButton>
        </Tooltip>
        <Tooltip title={"Queue Script"}>
            <IconButton
                onClick={() => setParamsModalOpen(true)}>
                <QueueIcon fontSize={"small"}/>
            </IconButton>
        </Tooltip>
        <div>
            <Typography variant={"caption"}>{formatScriptState(scriptState)}</Typography>
        </div>
        <div className={styles.progressIndicator}>
            <Typography variant={"caption"}>{formatElapsedTime(scriptElapsedTime)}</Typography>
            <LinearProgress
                variant={
                    scriptState !== ScriptState.RUNNING
                    && scriptState !== ScriptState.SHUTTING_DOWN ? "determinate" : "indeterminate"
                }
                value={0}
                className={styles.progressIndicatorLine}/>
            <Typography variant={"caption"}>--:--</Typography>

        </div>
        {paramsModalOpen && <ScriptParamsModal
            isOpen={paramsModalOpen}
            onClose={(scriptName, params, usernames) => {
                setParamsModalOpen(false);
                usernames?.forEach(player => {
                    loadScript(player, {
                        name: scriptName,
                        parameters: params
                    }).then(console.log).catch(console.error)
                })
            }}
            onCancel={() => setParamsModalOpen(false)}
            username={username}
        />}
        {viewStatsModalOpen && <StatsPopover
            isOpen={viewStatsModalOpen}
            username={username}
            onClose={() => setViewStatsModalOpen(false)}
            anchorEl={viewStatsEl}
        />
        }
    </div>;
}

function formatElapsedTime(ms: number): string {
    const seconds = Math.floor((ms / 1000) % 60);
    const minutes = Math.floor((ms / 1000 / 60) % 60);
    const hours = Math.floor((ms / 1000 / 3600) % 24);
    const days = Math.floor((ms / 1000 / 3600 / 24));

    if (days > 0) {
        return `${days}d ${hours}h ${minutes}m`
    } else {
        return `${pad(hours)}:${pad(minutes)}:${pad(seconds)}`
    }
}

function pad(unit: number): string {
    if (unit < 10) {
        return "0" + unit;
    }
    return "" + unit;
}