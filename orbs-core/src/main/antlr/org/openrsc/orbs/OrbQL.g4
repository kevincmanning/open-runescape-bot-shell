grammar OrbQL;

@header {
package org.openrsc.orbs;
}

expr: LPAREN expr RPAREN      #wrappedExpression
   | '!' expr                #notExpression
   | literal                 #literalExpression
   | functionCall            #funcExpression
   | expr op expr            #comparisonExpression
   | expr andor expr         #andOrExpression;


functionCall: (group=ID'.')?name=ID LPAREN args? RPAREN;

args: (expr COMMA)* expr;

literal: NUMBER #numberLiteral | bool #booleanLiteral | STRING #stringLiteral;

bool: 'true' | 'false';

op : '='|'>='|'>'|'<='|'<'|'!=';

andor: 'and' | 'or';

COMMA: ',';
LPAREN: '(';
RPAREN: ')';
WS : [ \t\r\n]+ -> skip ;
NUMBER : [0-9]+;
ID : [a-zA-Z0-9_|]+;
STRING : '"'(ESC|.)*? '"';
fragment
ESC: '\\"' | '\\\\';