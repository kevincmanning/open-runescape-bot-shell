package org.openrsc.orbs.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PlayerLoginRequestDTO {
    private final String username;
    private final String password;
}
