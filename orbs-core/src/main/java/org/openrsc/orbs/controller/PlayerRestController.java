package org.openrsc.orbs.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.openrsc.orbs.controller.dto.ScriptLoadRequestDTO;
import org.openrsc.orbs.controller.dto.StatDTO;
import org.openrsc.orbs.controller.dto.StatsDTO;
import org.openrsc.orbs.model.Skill;
import org.openrsc.orbs.net.connection.player.PlayerConnection;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptRepository;
import org.openrsc.orbs.orbql.OrbQLQueryManager;
import org.openrsc.orbs.service.PlayerService;
import org.openrsc.orbs.stateful.player.PlayerState;
import org.openrsc.orbs.stateful.skills.SkillState;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@RestController
@RequestMapping("/player")
public class PlayerRestController {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final PlayerService playerService;
    private final ScriptRepository scriptRepository;
    private final OrbQLQueryManager orbQLQueryManager;

    public PlayerRestController(
            PlayerService playerService,
            ScriptRepository scriptRepository,
            OrbQLQueryManager orbQLQueryManager) {
        this.playerService = playerService;
        this.scriptRepository = scriptRepository;
        this.orbQLQueryManager = orbQLQueryManager;
    }

    @GetMapping("/{name}/state")
    public PlayerState getPlayerState(@PathVariable String name) {
        return playerService.getPlayerState(name);
    }

    @GetMapping("/{name}/login")
    public boolean login(@PathVariable String name) {
        playerService.login(name);
        return true;
    }

    @GetMapping("/{name}/logout")
    public boolean logout(@PathVariable String name) {
        playerService.logout(name);
        return true;
    }

    @GetMapping("/{name}/script/start")
    public boolean startScript(@PathVariable String name) {
        try {
            final PlayerConnection playerConnection = playerService.getConnection(name);
            playerConnection.startScript();
            return true;
        } catch (Exception ex) {
            log.catching(ex);
            return false;
        }
    }

    @GetMapping("/{name}/script/pause")
    public boolean pauseScript(@PathVariable String name) {
        try {
            final PlayerConnection playerConnection = playerService.getConnection(name);
            playerConnection.pauseScript();
            return true;
        } catch (Exception ex) {
            log.catching(ex);
            return false;
        }
    }

    @GetMapping("/{name}/script/stop")
    public boolean stopScript(@PathVariable String name) {
        try {
            final PlayerConnection playerConnection = playerService.getConnection(name);
            playerConnection.shutdownScript();
            return true;
        } catch (Exception ex) {
            log.catching(ex);
            return false;
        }
    }

    @PostMapping("/{name}/script/load")
    @SuppressWarnings("unchecked")
    public boolean queueScript(
            @PathVariable String name,
            @RequestBody ScriptLoadRequestDTO request
    ) {
        try {
            final PlayerConnection playerConnection = playerService.getConnection(name);
            Class<? extends Script> script = scriptRepository.getClass(request.getName());
            Type parameterType = ((ParameterizedType) script.getGenericSuperclass()).getActualTypeArguments()[0];
            Object params = objectMapper.convertValue(request.getParameters(), Class.forName(parameterType.getTypeName()));
            playerConnection.queueScript(script, params);
            return true;
        } catch (Exception ex) {
            log.catching(ex);
            return false;
        }
    }

    @GetMapping("/{name}/stats")
    public StatsDTO getStats(
            @PathVariable String name
    ) {
        final PlayerState playerState = playerService.getPlayerState(name);
        final SkillState skills = playerState.getSkills();
        final Map<Skill, Integer> currentLevels = skills.getCurrentLevels();
        final Map<Skill, Integer> maxLevels = skills.getMaxLevels();

        final List<StatDTO> stats = Arrays.stream(Skill.values()).map(
                skill -> {
                    if (currentLevels.containsKey(skill)) {
                        return StatDTO.builder()
                                .name(StringUtils.capitalize(skill.name().toLowerCase()))
                                .currentLvl(currentLevels.get(skill))
                                .maxLvl(maxLevels.get(skill))
                                .build();
                    }
                    return null;
                }
        ).filter(Objects::nonNull).collect(Collectors.toList());

        return StatsDTO.builder().stats(stats).build();
    }

    @GetMapping("/dryRunQuery")
    public Map<String, Object> dryRunQuery(@RequestParam("query") String query) {
        try {
            return orbQLQueryManager.dryRun(query);
        } catch (Exception ex) {
            return null;
        }
    }

    @GetMapping
    public List<String> processQuery(@RequestParam("query") String query) {
        return orbQLQueryManager.findMatchingPlayers(query);
    }
}
