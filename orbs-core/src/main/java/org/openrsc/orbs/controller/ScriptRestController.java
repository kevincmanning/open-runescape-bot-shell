package org.openrsc.orbs.controller;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.openrsc.orbs.controller.dto.ScriptDefinitionDTO;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptRepository;
import org.openrsc.orbs.net.connection.player.script.params.ScriptParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.InvocationTargetException;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@RestController
@RequestMapping("/scripts")
@Log4j2
@SuppressWarnings("unchecked")
public class ScriptRestController {
    private final ScriptRepository scriptRepository;

    public ScriptRestController(ScriptRepository scriptRepository) {
        this.scriptRepository = scriptRepository;
    }

    @GetMapping
    public Set<ScriptDefinitionDTO<?>> getScripts() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        Set<ScriptDefinitionDTO<?>> scripts = new TreeSet<>(Comparator.comparing(ScriptDefinitionDTO::getName));
        for (Pair<String, Class<? extends Script>> scriptDef : scriptRepository.getScripts()) {
            Script<?> instance = scriptRepository.getNullAPIInstance(scriptDef.getKey());
            final Object defaultParams = instance.getDefaultParameters();
            final List<ScriptParam> allParams = instance.getOptions();

            scripts.add(
                    ScriptDefinitionDTO.builder().name(scriptDef.getKey())
                            .defaultParameters(defaultParams)
                            .options(allParams)
                            .build()
            );
        }
        return scripts;
    }
}
