package org.openrsc.orbs.controller.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder(toBuilder = true)
@Getter
public class AppearanceDTO {
    private final List<Integer> wornItemAppearanceIds;
    private final int head;
    private final int body;
    private final int hair;
    private final int top;
    private final int legs;
    private final int skin;
}
