package org.openrsc.orbs.controller;

import org.openrsc.orbs.controller.dto.ManagedPlayerDTO;
import org.openrsc.orbs.model.persisted.GroupDefinition;
import org.openrsc.orbs.model.persisted.PlayerDefinition;
import org.openrsc.orbs.service.PlayerService;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Collection;

@Controller
public class PlayerWSController {
    private final PlayerService playerService;

    public PlayerWSController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @MessageMapping("/managedPlayers")
    @SendTo("/topic/managedPlayers")
    public Collection<ManagedPlayerDTO> getManagedPlayers() {
        return playerService.getManagedPlayers();
    }

    @MessageMapping("/players")
    @SendTo("/topic/players")
    public Collection<PlayerDefinition> getPlayers() {
        return playerService.getAllPlayers();
    }

    @MessageMapping("/groups")
    @SendTo("/topic/groups")
    public Collection<GroupDefinition> getGroups() {
        return playerService.getAllGroups();
    }
}
