package org.openrsc.orbs.controller.dto;

public enum ConnectionStateDTO {
    FATAL_ERROR,
    RECONNECTING,
    LOGGING_IN,
    LOGGED_IN
}
