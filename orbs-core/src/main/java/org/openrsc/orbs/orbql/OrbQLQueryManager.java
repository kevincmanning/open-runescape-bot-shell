package org.openrsc.orbs.orbql;

import org.apache.commons.lang3.StringUtils;
import org.openrsc.orbs.orbql.model.Expression;
import org.openrsc.orbs.orbql.model.FunctionCall;
import org.openrsc.orbs.orbql.model.Operator;
import org.openrsc.orbs.service.PlayerService;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class OrbQLQueryManager {
    private final PlayerService playerService;
    private final OrbQLApi api;
    private final OrbQLEvaluator evaluator;

    public OrbQLQueryManager(PlayerService playerService, OrbQLApi api, OrbQLEvaluator evaluator) {
        this.playerService = playerService;
        this.api = api;
        this.evaluator = evaluator;
    }

    public List<String> findMatchingPlayers(String query) {
        final Set<String> players = playerService.getPlayers();
        final Expression expression = evaluator.evalExpressionInput(query);
        return players.stream()
                .filter(username -> evalBooleanExpression(expression, username))
                .collect(Collectors.toList());
    }

    private Boolean evalBooleanExpression(Expression expression, String username) {
        return booleanLiteral(evalExpression(expression, username));
    }

    private Object evalExpression(Expression expression, String username) {
        switch (expression.getType()) {
            case LITERAL:
                return expression.getLiteralValue();
            case WRAPPED:
                return evalExpression(expression.getFirst(), username);
            case NEGATION:
                return !evalBooleanExpression(expression.getFirst(), username);
            case COMPARISON:
                return compare(
                        evalExpression(expression.getFirst(), username),
                        evalExpression(expression.getSecond(), username),
                        expression.getOperator(),
                        username
                );
            case FUNCTION_CALL:
                return callRetrievalFunction(expression.getFunctionCall(), username);
            default:
                return false;
        }
    }

    private boolean compare(
            Object first,
            Object second,
            Operator operator,
            String username
    ) {
        if (first instanceof Expression) {
            first = evalExpression((Expression) first, username);
        }
        if (second instanceof Expression) {
            second = evalExpression((Expression) second, username);
        }
        int firstNum = -1;
        int secondNum = -1;
        if (Operator.NUMBER_OPERATORS.contains(operator)) {
            validateNumbers(first, second);
            firstNum = ((Number) first).intValue();
            secondNum = ((Number) second).intValue();
        }

        switch (operator) {
            case GT:
                return firstNum > secondNum;
            case GT_EQ:
                return firstNum >= secondNum;
            case LT:
                return firstNum < secondNum;
            case LT_EQ:
                return firstNum <= secondNum;
            case EQ:
                return first.equals(second);
            case NEQ:
                return !first.equals(second);
            case OR:
                return booleanLiteral(first) || booleanLiteral(second);
            case AND:
                return booleanLiteral(first) && booleanLiteral(second);
            default:
                return false;
        }
    }

    private void validateNumbers(Object first, Object second) {
        if (!(first instanceof Number) || !(second instanceof Number)) {
            throw new IllegalStateException("Unable to compare non-numbers with number-specific operator.");
        }
    }

    private Boolean booleanLiteral(Object literal) {
        if (literal instanceof String) {
            return StringUtils.isNotEmpty((String) literal);
        } else if (literal instanceof Number) {
            return ((Number) literal).intValue() > 0;
        } else if (literal instanceof Boolean) {
            return (Boolean) literal;
        }
        return false;
    }

    private Object callRetrievalFunction(FunctionCall functionCall, String username) {
        assert functionCall.getGroup() == null;
        final String functionName = functionCall.getFunctionName();
        assert functionName != null;

        final List<Object> functionParams = functionCall.getParams();
        final List<Object> params = new ArrayList<>(functionParams.size() + 1);
        params.add(username);

        for (Object object : functionParams) {
            if (object instanceof Expression) {
                params.add(evalExpression((Expression) object, username));
            } else {
                params.add(object);
            }
        }
        Class<?>[] paramTypes = params
                .stream()
                .map(Object::getClass)
                .toArray(Class<?>[]::new);
        try {
            final Method method = api.getClass().getMethod(functionName, paramTypes);
            return method.invoke(api, params.toArray());
        } catch (NoSuchMethodException noSuchMethodException) {
            throw new IllegalArgumentException(
                    MessageFormat.format("Method '{0}' not found for argument types: [{1}]",
                            functionName,
                            StringUtils.join(paramTypes, ",")
                    )
            );
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(
                    MessageFormat.format("Method '{0}' not accessible.", functionName)
            );
        } catch (InvocationTargetException e) {
            throw new IllegalStateException(
                    MessageFormat.format("Error executing method '{0}'", functionName),
                    e
            );
        }
    }

    public Map<String, Object> dryRun(String query) {
        return null;
    }
}
