package org.openrsc.orbs.orbql;

import org.openrsc.orbs.model.Skill;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.service.PlayerService;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.function.Function;

@Component
public class OrbQLApi {
    private final PlayerService playerService;

    public OrbQLApi(PlayerService playerService) {
        this.playerService = playerService;
    }

    public Integer level(String username, String skill) {
        final String uppercaseSkill = skill.toUpperCase(Locale.ROOT);
        return withScriptApi(
                username,
                api -> api.getMaxLevel(Skill.valueOf(uppercaseSkill))
        );
    }

    public Integer fatigue(String username) {
        return withScriptApi(username, ScriptAPI::getFatigue);
    }

    public Integer bank(String username, Integer itemId) {
        return withScriptApi(username, api -> api.getBankCount(itemId));
    }

    public Integer inventory(String username) {
        return withScriptApi(username, api -> api.getInventory().size());
    }

    public Integer inventory(String username, Integer itemId) {
        return withScriptApi(username, api -> api.getInventoryCount(itemId));
    }

    public Integer inventory(String username, Integer... itemId) {
        return withScriptApi(username, api -> api.getInventoryCount(itemId));
    }

    private Integer withScriptApi(String username, Function<ScriptAPI, Integer> func) {
        try {
            return func.apply(playerService.getPlayerApi(username));
        } catch (Exception ex) {
            return -1;
        }
    }
}
