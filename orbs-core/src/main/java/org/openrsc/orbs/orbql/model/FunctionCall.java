package org.openrsc.orbs.orbql.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;

import java.util.List;

@Builder
@Getter
@ToString
public class FunctionCall extends PqlStatement {
    private final String group;
    private final String functionName;
    @Singular
    private final List<Object> params;
}
