package org.openrsc.orbs.orbql.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@ToString
public class Expression extends PqlStatement {
    private final ExpressionType type;
    private final Expression first;
    private final Expression second;
    private final Operator operator;
    private final FunctionCall functionCall;
    private final Object literalValue;
}
