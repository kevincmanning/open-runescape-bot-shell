package org.openrsc.orbs.orbql;

import lombok.extern.log4j.Log4j2;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.openrsc.orbs.OrbQLLexer;
import org.openrsc.orbs.OrbQLParser;
import org.openrsc.orbs.orbql.model.Expression;
import org.openrsc.orbs.orbql.model.ExpressionType;
import org.openrsc.orbs.orbql.model.FunctionCall;
import org.openrsc.orbs.orbql.model.Operator;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@Log4j2
public class OrbQLEvaluator {

    public Expression evalExpressionInput(String input) {
        CharStream charStream = new ANTLRInputStream(input);
        final OrbQLLexer orbQLLexer = new OrbQLLexer(charStream);
        final CommonTokenStream commonTokenStream = new CommonTokenStream(orbQLLexer);
        final OrbQLParser orbQLParser = new OrbQLParser(commonTokenStream);

        return evalExpression(orbQLParser.expr());
    }

    public FunctionCall evalFunction(OrbQLParser.FunctionCallContext functionCall) {
        final FunctionCall.FunctionCallBuilder builder = FunctionCall.builder();
        if (functionCall.group != null) {
            builder.group(functionCall.group.getText());
        }
        builder.functionName(functionCall.name.getText());
        builder.params(evalArguments(functionCall.args()));
        return builder.build();
    }

    public List<Object> evalArguments(OrbQLParser.ArgsContext args) {
        if (args == null) {
            return Collections.emptyList();
        }
        List<Object> params = new ArrayList<>();
        for (int i = 0; true; i++) {
            final OrbQLParser.ExprContext expr = args.expr(i);
            if (expr != null) {
                params.add(evalExpression(expr));
            } else {
                break;
            }
        }
        return params;
    }

    public Expression evalExpression(OrbQLParser.ExprContext expression) {
        if (expression instanceof OrbQLParser.ComparisonExpressionContext) {
            final OrbQLParser.ComparisonExpressionContext comparison = (OrbQLParser.ComparisonExpressionContext) expression;
            return Expression.builder()
                    .type(ExpressionType.COMPARISON)
                    .first(evalExpression(comparison.expr(0)))
                    .second(evalExpression(comparison.expr(1)))
                    .operator(evalOperator(comparison.op()))
                    .build();
        } else if (expression instanceof OrbQLParser.AndOrExpressionContext) {
            final OrbQLParser.AndOrExpressionContext joining = (OrbQLParser.AndOrExpressionContext) expression;
            return Expression.builder()
                    .type(ExpressionType.COMPARISON)
                    .first(evalExpression(joining.expr(0)))
                    .second(evalExpression(joining.expr(1)))
                    .operator(evalOperator(joining.andor()))
                    .build();
        } else if (expression instanceof OrbQLParser.NotExpressionContext) {
            final OrbQLParser.NotExpressionContext not = (OrbQLParser.NotExpressionContext) expression;
            return Expression.builder()
                    .type(ExpressionType.NEGATION)
                    .first(evalExpression(not.expr()))
                    .build();
        } else if (expression instanceof OrbQLParser.WrappedExpressionContext) {
            final OrbQLParser.WrappedExpressionContext wrapped = (OrbQLParser.WrappedExpressionContext) expression;
            return Expression.builder()
                    .type(ExpressionType.WRAPPED)
                    .first(evalExpression(wrapped.expr()))
                    .build();
        } else if (expression instanceof OrbQLParser.FuncExpressionContext) {
            final OrbQLParser.FuncExpressionContext function = (OrbQLParser.FuncExpressionContext) expression;
            return Expression.builder()
                    .type(ExpressionType.FUNCTION_CALL)
                    .functionCall(evalFunction(function.functionCall()))
                    .build();
        } else if (expression instanceof OrbQLParser.LiteralExpressionContext) {
            final OrbQLParser.LiteralExpressionContext literal = (OrbQLParser.LiteralExpressionContext) expression;
            return Expression.builder()
                    .type(ExpressionType.LITERAL)
                    .literalValue(evalLiteral(literal.literal()))
                    .build();
        }
        return null;
    }

    public Operator evalOperator(OrbQLParser.OpContext operator) {
        return Operator.of(operator.getText());
    }

    public Operator evalOperator(OrbQLParser.AndorContext operator) {
        return Operator.of(operator.getText());
    }

    public Object evalLiteral(OrbQLParser.LiteralContext literal) {
        final String text = literal.getText();
        if (literal instanceof OrbQLParser.NumberLiteralContext) {
            return Integer.parseInt(text);
        } else if (literal instanceof OrbQLParser.BooleanLiteralContext) {
            return Boolean.valueOf(text);
        } else if (literal instanceof OrbQLParser.StringLiteralContext) {
            return text.substring(1, text.length() - 1);
        }
        return null;
    }
}
