package org.openrsc.orbs.stateful.player.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerIcon;

import java.util.List;

@Builder(toBuilder = true)
@Getter
@ToString
public class Player {
    private final Integer playerId;

    // Appearance info
    private final String username;
    private final List<Integer> wornItemAppearanceIds;
    private final byte hairColor;
    private final byte topColor;
    private final byte pantsColor;
    private final byte skinColor;
    private final Integer combatLevel;
    private final boolean skulled;
    private final String clanTag;
    private final boolean invisible;
    private final boolean invulnerable;
    private final Integer groupId;
    private final PlayerIcon icon;

    // Hp info
    private final Integer currentHp;
    private final Integer maxHp;
}
