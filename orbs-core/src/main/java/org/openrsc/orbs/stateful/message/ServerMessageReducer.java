package org.openrsc.orbs.stateful.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.extern.log4j.Log4j2;
import org.mapstruct.factory.Mappers;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.connection.player.script.event.DuelRequest;
import org.openrsc.orbs.net.connection.player.script.event.QuestMessage;
import org.openrsc.orbs.net.connection.player.script.event.TradeRequest;
import org.openrsc.orbs.net.dto.server.message.ServerMessageEvent;
import org.openrsc.orbs.net.mapper.script.PlayerMapper;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.PlayerState;

import java.text.MessageFormat;
import java.util.HashMap;

@Log4j2
public class ServerMessageReducer extends AbstractReducer {
    private final EventBus scriptEventBus;
    private final PlayerState playerState;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final PlayerMapper playerMapper = Mappers.getMapper(PlayerMapper.class);

    public ServerMessageReducer(PlayerState playerState, EventBus eventBus, EventBus scriptEventBus) {
        super(eventBus);
        this.scriptEventBus = scriptEventBus;
        this.playerState = playerState;
    }

    @Subscribe
    public void onServerMessageEvent(ServerMessageEvent event) throws JsonProcessingException {
        final MessageHistory messageHistory = playerState.getMessageHistory();
        switch (event.getMessageType()) {
            case TRADE -> {
                String username = event.getSender();
                new HashMap<>(playerState.getPlayerInfoByPid())
                        .values()
                        .stream()
                        .filter(player -> player.getUsername().equalsIgnoreCase(username))
                        .findAny()
                        .ifPresent(player -> {
                            Point location = playerState.getPlayersByPid().get(player.getPlayerId());
                            scriptEventBus.post(new TradeRequest(
                                    playerMapper.toScriptDTO(player, location)
                            ));
                        });
                log.info(MessageFormat.format("Trade request from {0}", username));
                messageHistory.add(event.getMessage());
            }
            case INVENTORY -> {
                String message = event.getMessage();
                if (message.endsWith("wishes to duel with you")) {
                    String username = message.substring(0, message.indexOf('@') - 1);
                    new HashMap<>(playerState.getPlayerInfoByPid())
                            .values()
                            .stream()
                            .filter(player -> player.getUsername().equalsIgnoreCase(username))
                            .findAny()
                            .ifPresent(player -> scriptEventBus.post(new DuelRequest(player)));
                    log.info(MessageFormat.format("Duel request from {0}", username));
                }
                messageHistory.add(message);
            }
            case QUEST -> {
                String message = event.getMessage();
                messageHistory.add(message);
                scriptEventBus.post(new QuestMessage(message));
            }
            default -> {
                log.debug("Unhandled server message: " + objectMapper.writeValueAsString(event));
            }
        }
    }
}
