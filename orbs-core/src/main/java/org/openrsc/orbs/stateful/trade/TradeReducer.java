package org.openrsc.orbs.stateful.trade;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.dto.server.trade.PlayerAcceptedTradeEvent;
import org.openrsc.orbs.net.dto.server.trade.SelfAcceptedTradeEvent;
import org.openrsc.orbs.net.dto.server.trade.SendOpenTradeConfirmWindowEvent;
import org.openrsc.orbs.net.dto.server.trade.SendTradeWindowEvent;
import org.openrsc.orbs.net.dto.server.trade.TradeCloseWindowEvent;
import org.openrsc.orbs.net.dto.server.trade.TradeOfferUpdateEvent;
import org.openrsc.orbs.stateful.AbstractReducer;

import java.util.ArrayList;

@Log4j2
public class TradeReducer extends AbstractReducer {
    private final TradeState tradeState;

    public TradeReducer(EventBus eventBus, TradeState tradeState) {
        super(eventBus);
        this.tradeState = tradeState;
    }

    @Subscribe
    public void onOtherPlayerAcceptedTradeEvent(PlayerAcceptedTradeEvent event) {
        log.info(event);
        tradeState.setOtherPlayerAccepted(event.isAccepted());
    }

    @Subscribe
    public void onSelfAcceptedTradeEvent(SelfAcceptedTradeEvent event) {
        log.info(event);
        tradeState.setAccepted(event.isAccepted());
    }

    @Subscribe
    public void onOpenTradeConfirmWindowEvent(SendOpenTradeConfirmWindowEvent event) {
        log.info(event);
        tradeState.setStatus(TradeStatus.CONFIRM_WINDOW);
        tradeState.setOtherPlayerOfferedItems(event.getReceivedItems());
        tradeState.setOfferedItems(event.getOfferedItems());
    }

    @Subscribe
    public void onOpenTradeWindowEvent(SendTradeWindowEvent event) {
        log.info(event);
        tradeState.setStatus(TradeStatus.OFFER_WINDOW);
        tradeState.setOtherPlayerPlayerId(event.getPlayerId());
    }

    @Subscribe
    public void onTradeOfferUpdateEvent(TradeOfferUpdateEvent event) {
        log.info(event);
        tradeState.setOtherPlayerAccepted(false);
        tradeState.setAccepted(false);
        tradeState.setOfferedItems(event.getOfferedItems());
        tradeState.setOtherPlayerOfferedItems(event.getReceivedItems());
    }

    @Subscribe
    public void onTradeCloseWindowEvent(TradeCloseWindowEvent event) {
        log.info(event);
        tradeState.setStatus(TradeStatus.NOT_TRADING);
        tradeState.setAccepted(false);
        tradeState.setOtherPlayerAccepted(false);
        tradeState.setOfferedItems(new ArrayList<>());
        tradeState.setOtherPlayerOfferedItems(new ArrayList<>());
    }
}
