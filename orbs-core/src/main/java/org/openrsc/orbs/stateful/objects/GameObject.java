package org.openrsc.orbs.stateful.objects;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.openrsc.orbs.model.Locatable;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.dto.GameObjectLoc;

@Builder(toBuilder = true)
@Getter
@EqualsAndHashCode
@ToString
public class GameObject implements Locatable {
    private final int id;
    private final Point location;
    private final int direction;
    private final int walkingDistance;

    public static GameObject fromDTO(GameObjectLoc loc, Point actualLocation) {
        return GameObject.builder()
                .id(loc.getId())
                .location(actualLocation)
                .direction(loc.getDirection())
                .build();
    }
}
