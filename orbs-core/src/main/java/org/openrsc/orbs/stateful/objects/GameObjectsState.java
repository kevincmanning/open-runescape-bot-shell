package org.openrsc.orbs.stateful.objects;

import lombok.Getter;
import org.openrsc.orbs.model.Point;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
public class GameObjectsState {
    private final Map<Point, GameObject> boundaries = new ConcurrentHashMap<>();
    private final Map<Point, GameObject> sceneries = new ConcurrentHashMap<>();
}
