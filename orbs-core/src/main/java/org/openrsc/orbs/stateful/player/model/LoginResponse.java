package org.openrsc.orbs.stateful.player.model;

import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Getter
public enum LoginResponse {
    SERVER_TIMEOUT("Unable to connect", -1),
    LOGIN_UNSUCCESSFUL("Login unsuccessful", 0),
    RECONNECT_SUCCESFUL("Reconnect successful", 1),
    UNRECOGNIZED_LOGIN("Unrecognized login", 2),
    INVALID_CREDENTIALS("Invalid credentials", 3),
    ACCOUNT_LOGGEDIN("User is already logged in", 4),
    CLIENT_UPDATED("Client updated. Please contact a developer", 5),
    IP_IN_USE("IP already in use", 6),
    LOGIN_ATTEMPTS_EXCEEDED("Login attempts exceeded", 7),
    SERVER_REJECT("Server rejected", 8),
    UNDER_13_YEARS_OLD("Under 13 years old", 9),
    USERNAME_ALREADY_IN_USE("Username already in use", 10),
    ACCOUNT_TEMP_DISABLED("Account temporarily disabled", 11),
    ACCOUNT_PERM_DISABLED("Account permanently disabled", 12),
    WORLD_IS_FULL("World is full", 14),
    NEED_MEMBERS_ACCOUNT("A members account is needed for this server", 15),
    LOGINSERVER_OFFLINE("Login server offline", 16),
    FAILED_TO_DECODE_PROFILE("Failed to decode profile", 17),
    ACCOUNT_SUSPECTED_STOLEN("Account is suspected to be stolen", 18),
    LOGINSERVER_MISMATCH("Login server mismatch", 20),
    NOT_VETERAN_ACCOUNT("Not a veteran account", 21),
    PASSWORD_STOLEN("Password stolen", 22),
    NEED_TO_SET_DISPLAY_NAME("You need to set a display name", 23),
    WORLD_DOES_NOT_ACCEPT_NEW_PLAYERS("This world is not accepting new players", 24),
    NONE_OF_YOUR_CHARACTERS_CAN_LOGIN("None of your characters can login", 25),
    LOGIN_SUCCESSFUL("Login successful", 87, 86, 83, 82, 89, 89, 88, 88, 85, 84, 64, 89);

    private static final Map<Integer, LoginResponse> byCode = new HashMap<>();

    static {
        for (LoginResponse response : values()) {
            Arrays.stream(response.responseCodes).forEach(code -> byCode.put(code, response));
        }
    }

    private final int[] responseCodes;
    private final String message;

    LoginResponse(String message, int... responseCodes) {
        this.message = message;
        this.responseCodes = responseCodes;
    }

    public static LoginResponse fromResponseCode(int code) {
        return byCode.get(code);
    }

    public boolean isSuccess() {
        return this == LOGIN_SUCCESSFUL;
    }

    public String getMessage() {
        return message;
    }
}
