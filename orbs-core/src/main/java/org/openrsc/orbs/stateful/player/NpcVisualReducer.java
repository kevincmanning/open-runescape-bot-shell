package org.openrsc.orbs.stateful.player;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import lombok.Synchronized;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcBubbleEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcChatEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcDamageEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcOnNpcProjectileEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcOnPlayerProjectileEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcSkullEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcUpdateEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcVisualUpdateEvent;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.model.NpcInfo;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

public class NpcVisualReducer extends AbstractReducer {
    private final Map<Integer, NpcInfo> npcInfoByServerId;
    private final EventBus eventBus;

    public NpcVisualReducer(Map<Integer, NpcInfo> npcInfoByServerId, EventBus eventBus) {
        super(eventBus);
        this.npcInfoByServerId = npcInfoByServerId;
        this.eventBus = eventBus;
    }

    @Subscribe
    public void onNpcVisualUpdateEvent(NpcVisualUpdateEvent event) {
        for (NpcUpdateEvent updateEvent : event.getUpdateEvents()) {
            eventBus.post(updateEvent);
        }
    }

    @Subscribe
    public void onChatMessageEvent(NpcChatEvent event) {
        // on chat message event for script?
    }

    @Subscribe
    public void onNpcDamageEvent(NpcDamageEvent event) {
        // on damage event for script?
        updateNpc(
                event,
                npcInfo -> npcInfo.toBuilder()
                        .serverId(event.getServerIndex())
                        .currentHp(event.getCurrentHp())
                        .maxHp(event.getMaxHp())
                        .build()
        );
    }

    @Subscribe
    public void onNpcNpcProjectileEvent(NpcOnNpcProjectileEvent event) {
        // ignore for now
    }

    @Subscribe
    public void onNpcPlayerProjectileEvent(NpcOnPlayerProjectileEvent event) {
        // ignore for now
    }

    @Subscribe
    public void onNpcSkullEvent(NpcSkullEvent event) {
        updateNpc(
                event,
                npcInfo -> npcInfo.toBuilder()
                        .serverId(event.getServerIndex())
                        .isSkulled(event.isSkulled())
                        .build()
        );
    }

    @Subscribe
    public void onNpcBubbleEvent(NpcBubbleEvent event) {
        // ignore for now
    }

    @Synchronized
    public void updateNpc(NpcUpdateEvent event, Function<NpcInfo, NpcInfo> npcUpdater) {
        int serverId = event.getServerIndex();
        NpcInfo current = Optional.ofNullable(npcInfoByServerId.get(serverId)).orElse(NpcInfo.builder().build());
        npcInfoByServerId.put(serverId, npcUpdater.apply(current));
    }
}
