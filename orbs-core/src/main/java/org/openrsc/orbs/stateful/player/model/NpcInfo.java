package org.openrsc.orbs.stateful.player.model;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class NpcInfo {
    private final int serverId;
    private final int currentHp;
    private final int maxHp;
    private final boolean isSkulled;
}
