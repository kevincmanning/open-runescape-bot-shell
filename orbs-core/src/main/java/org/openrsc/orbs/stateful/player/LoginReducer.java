package org.openrsc.orbs.stateful.player;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.openrsc.orbs.net.dto.server.login.LoginEvent;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.model.LoginResponse;

public class LoginReducer extends AbstractReducer {
    private final PlayerState playerState;
    private final EventBus scriptEventBus;

    public LoginReducer(PlayerState playerState, EventBus eventBus, EventBus scriptEventBus) {
        super(eventBus);
        this.playerState = playerState;
        this.scriptEventBus = scriptEventBus;
    }

    @Subscribe
    public void onLoginEvent(LoginEvent event) {
        final LoginResponse loginResponse = event.getLoginResponse();
        playerState.getMessageHistory().add(loginResponse.getMessage());
        scriptEventBus.post(event);
    }
}
