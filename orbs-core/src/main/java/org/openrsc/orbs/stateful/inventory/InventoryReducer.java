package org.openrsc.orbs.stateful.inventory;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.openrsc.orbs.model.InventoryItem;
import org.openrsc.orbs.net.dto.server.inventory.InventoryRemoveEvent;
import org.openrsc.orbs.net.dto.server.inventory.InventoryUpdateEvent;
import org.openrsc.orbs.net.dto.server.inventory.SendInventoryEvent;
import org.openrsc.orbs.stateful.AbstractReducer;

import java.util.List;

public class InventoryReducer extends AbstractReducer {
    private final InventoryState inventoryState;

    public InventoryReducer(InventoryState inventoryState, EventBus eventBus) {
        super(eventBus);
        this.inventoryState = inventoryState;
    }

    @Subscribe
    public void onSendInventoryEvent(SendInventoryEvent sendInventoryEvent) {
        List<InventoryItem> updatedItems = sendInventoryEvent.getInventoryItems();

        List<InventoryItem> inventoryItems = inventoryState.getBySlot();
        inventoryItems.clear();
        inventoryItems.addAll(updatedItems);

        //TODO: Does this work for non-noted items?
//        Map<Integer, InventoryItem> inventoryItemsById = inventoryState.getById();
//        inventoryItemsById.clear();
//        updatedItems.forEach(item -> inventoryItemsById.put(item.getDef().getId(), item));
    }

    @Subscribe
    public void onInventoryUpdateEvent(InventoryUpdateEvent event) {
        List<InventoryItem> bySlot = inventoryState.getBySlot();
        if (event.getSlotId() >= bySlot.size()) {
            bySlot.add(event.getItem());
        } else {
            bySlot.set(
                    event.getSlotId(),
                    event.getItem()
            );
        }
    }

    @Subscribe
    public void onInventoryRemove(InventoryRemoveEvent event) {
        inventoryState.getBySlot().remove(event.getSlotId());
    }
}
