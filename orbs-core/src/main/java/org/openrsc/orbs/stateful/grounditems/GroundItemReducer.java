package org.openrsc.orbs.stateful.grounditems;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang3.tuple.Pair;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.dto.ItemLoc;
import org.openrsc.orbs.net.dto.server.grounditems.GroundItemUpdateEvent;
import org.openrsc.orbs.net.util.DataConversions;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.PlayerState;

public class GroundItemReducer extends AbstractReducer {
    private final PlayerState playerState;

    public GroundItemReducer(PlayerState playerState, EventBus eventBus) {
        super(eventBus);
        this.playerState = playerState;
    }

    @Subscribe
    public void onGroundItemUpdateEvent(GroundItemUpdateEvent event) {
        event.getRemovedItems().forEach(
                loc -> {
                    Pair<Integer, Point> translatedLoc = translateItemIdAndPosition(loc);
                    playerState.getGroundItems().remove(translatedLoc.getLeft(), translatedLoc.getRight());
                }
        );
        event.getItemLocs().forEach(
                loc -> {
                    Pair<Integer, Point> translatedLoc = translateItemIdAndPosition(loc);
                    playerState.getGroundItems().put(translatedLoc.getLeft(), translatedLoc.getRight());
                }
        );
    }

    public Pair<Integer, Point> translateItemIdAndPosition(ItemLoc loc) {
        Point location = DataConversions.reverseCoordOffsets(
                loc.getOffsetX(),
                loc.getOffsetY(),
                playerState.getLocation()
        );
        int itemId = loc.getId();
        return Pair.of(itemId, location);
    }
}
