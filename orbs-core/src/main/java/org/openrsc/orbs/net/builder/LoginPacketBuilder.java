package org.openrsc.orbs.net.builder;

import org.openrsc.orbs.net.dto.client.ClientLimitations;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.opcode.ClientOpcode;
import org.openrsc.orbs.net.serializer.DefaultPacketSerializer;
import org.openrsc.orbs.net.util.DataConversions;

public class LoginPacketBuilder {
    // Inauthentic client version
    private static final int INAUTHENTIC_CLIENT_VERSION = 10008;

    public Packet build(String username, String password) {
        // This is ignored by the server, just set false
        boolean reconnecting = false;

        PacketBuilder packetBuilder = new PacketBuilder();
        packetBuilder.setID(ClientOpcode.LOGIN.getOpCode());
        packetBuilder.writeByte(reconnecting ? 1 : 0);
        packetBuilder.writeInt(INAUTHENTIC_CLIENT_VERSION);
        packetBuilder.writeString(DataConversions.addCharacters(username, 10));
        packetBuilder.writeString(DataConversions.addCharacters(password, 10));
        // Random unused data
        packetBuilder.writeLong(0);

        packetBuilder.writeBytes(
                new DefaultPacketSerializer().serialize(ClientLimitations.getDefault()).getBuffer().array()
        );

        return packetBuilder.toPacket();
    }
}
