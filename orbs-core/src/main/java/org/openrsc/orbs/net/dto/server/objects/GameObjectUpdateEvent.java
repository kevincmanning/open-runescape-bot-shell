package org.openrsc.orbs.net.dto.server.objects;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.GameObjectLoc;
import org.openrsc.orbs.net.dto.server.Event;

import java.util.List;

@SuperBuilder
@Getter
public class GameObjectUpdateEvent extends Event {
    private final List<GameObjectLoc> objects;
}
