package org.openrsc.orbs.net.dto.client;

import lombok.Builder;
import lombok.Getter;
import org.openrsc.orbs.net.annotation.PacketSerializable;

@Builder
@Getter
@PacketSerializable
public class ClientLimitations {
    private final short maxAnimationId;
    private final int maxItemId;
    private final int maxNpcId;
    private final int maxSceneryId;
    private final short maxPrayerId;
    private final short maxSpellId;
    private final byte maxSkillId;
    private final short maxRoofId;
    private final short maxTextureId;
    private final short maxTileId;
    private final int maxBoundaryId;
    private final byte maxTeleBubbleId;
    private final short maxProjectileSprite;
    private final int maxSkinColor;
    private final int maxHairColor;
    private final int maxClothingColor;
    private final short maxQuestId;
    private final int numberOfSounds;
    private final byte supportsModSprites;
    private final byte maxDialogueOptions;
    private final int maxBankItems;
    private final String mapHash;

    public static ClientLimitations getDefault() {
        return ClientLimitations.builder()
                .maxAnimationId((short) 482)
                .maxItemId(1486)
                .maxNpcId(815)
                .maxSceneryId(1294)
                .maxPrayerId((short) 13)
                .maxSpellId((short) 47)
                .maxSkillId((byte) 19)
                .maxRoofId((short) 5)
                .maxTextureId((short) 54)
                .maxTileId((short) 24)
                .maxBoundaryId(213)
                .maxTeleBubbleId((byte) 1)
                .maxProjectileSprite((short) 6)
                .maxSkinColor(4)
                .maxHairColor(9)
                .maxClothingColor(14)
                .maxQuestId((short) 99)
                .numberOfSounds(37)
                .supportsModSprites((byte) 4)
                .maxDialogueOptions((byte) 20)
                .maxBankItems(Integer.MAX_VALUE)
                .mapHash("0dd0a1e47767f7f64b7931688131512f")
                .build();
    }
}
