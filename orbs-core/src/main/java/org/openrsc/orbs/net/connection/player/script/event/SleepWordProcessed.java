package org.openrsc.orbs.net.connection.player.script.event;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SleepWordProcessed {
    private final String sleepWord;
}
