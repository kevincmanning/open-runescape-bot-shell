package org.openrsc.orbs.net.connection.player.script.params;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class SelectableItem<L> extends SelectableValue<L> {
    private final Integer itemId;

    public SelectableItem(String label, L value, Integer itemId) {
        super(label, value);
        this.itemId = itemId;
    }

    public static <T extends Enum<T>> List<SelectableValue<T>> of(T[] values) {
        return Arrays.stream(values)
                .map(SelectableItem::of)
                .collect(Collectors.toList());
    }

    private static <T extends Enum<T>> SelectableItem<T> of(T value) {
        String label = value.toString();
        Integer itemId = ((Itemable) value).getItemId();
        return new SelectableItem<>(label, value, itemId);
    }
}
