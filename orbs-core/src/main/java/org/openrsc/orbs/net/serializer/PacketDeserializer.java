package org.openrsc.orbs.net.serializer;

import org.openrsc.orbs.net.model.Packet;

public interface PacketDeserializer<T> {
    T deserialize(Packet packet, Class<?> object);
}
