package org.openrsc.orbs.net.mapper.script;

import org.mapstruct.Mapper;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.connection.player.script.model.Player;

@Mapper
public interface PlayerMapper {
    Player toScriptDTO(
            org.openrsc.orbs.stateful.player.model.Player player,
            Point location
    );
}
