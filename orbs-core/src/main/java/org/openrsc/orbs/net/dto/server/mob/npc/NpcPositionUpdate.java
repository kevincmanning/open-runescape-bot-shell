package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class NpcPositionUpdate {
    private final int offsetX;
    private final int offsetY;
    private final int npcId;
}
