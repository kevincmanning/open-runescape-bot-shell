package org.openrsc.orbs.net.dto.server.objects;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.GameObjectUpdateEventDeserializer;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = GameObjectUpdateEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_SCENERY_HANDLER)
public class SceneryUpdateEvent extends GameObjectUpdateEvent {
}
