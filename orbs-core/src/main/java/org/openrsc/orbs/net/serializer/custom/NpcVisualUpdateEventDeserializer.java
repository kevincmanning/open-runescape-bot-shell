package org.openrsc.orbs.net.serializer.custom;

import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcBubbleEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcChatEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcDamageEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcOnNpcProjectileEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcOnPlayerProjectileEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcSkullEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcVisualUpdateEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.NpcWieldEvent;
import org.openrsc.orbs.net.dto.server.mob.npc.model.NpcUpdateType;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.ProjectileType;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

@Log4j2
public class NpcVisualUpdateEventDeserializer implements PacketDeserializer<NpcVisualUpdateEvent> {
    @Override
    public NpcVisualUpdateEvent deserialize(Packet packet, Class<?> type) {
        NpcVisualUpdateEvent.NpcVisualUpdateEventBuilder builder = NpcVisualUpdateEvent.builder();

        short numberOfUpdates = packet.readShort();
        for (int i = 0; i < numberOfUpdates; i++) {
            int serverIndex = packet.readShort();
            int updateTypeInt = Byte.toUnsignedInt(packet.readByte());
            NpcUpdateType updateType = NpcUpdateType.byId(updateTypeInt);
            if (updateType == null) {
                log.info("Unknown update type: " + updateTypeInt);
                continue;
            }
            switch (updateType) {
                case CHAT_MESSAGE -> {
                    builder.updateEvent(
                            NpcChatEvent.builder()
                                    .serverIndex(serverIndex)
                                    .recipientPlayerId(Short.toUnsignedInt(packet.readShort()))
                                    .message(packet.readString())
                                    .build()
                    );
                }
                case DAMAGE_UPDATE -> {
                    int damage = Byte.toUnsignedInt(packet.readByte());
                    int currentHp = Byte.toUnsignedInt(packet.readByte());
                    int maxHp = Byte.toUnsignedInt(packet.readByte());
                    builder.updateEvent(
                            NpcDamageEvent.builder()
                                    .serverIndex(serverIndex)
                                    .damage(damage)
                                    .currentHp(currentHp)
                                    .maxHp(maxHp)
                                    .build()
                    );
                }
                case PROJECTILE_NPC -> {
                    ProjectileType projectileType = ProjectileType.byId(packet.readShort());
                    int victimServerIndex = Short.toUnsignedInt(packet.readShort());
                    builder.updateEvent(
                            NpcOnNpcProjectileEvent.builder()
                                    .serverIndex(serverIndex)
                                    .projectileType(projectileType)
                                    .victimServerIndex(victimServerIndex)
                                    .build()
                    );
                }
                case PROJECTILE_PLAYER -> {
                    ProjectileType projectileType = ProjectileType.byId(packet.readShort());
                    int victimPid = Short.toUnsignedInt(packet.readShort());
                    builder.updateEvent(
                            NpcOnPlayerProjectileEvent.builder()
                                    .serverIndex(serverIndex)
                                    .projectileType(projectileType)
                                    .victimPid(victimPid)
                                    .build()
                    );
                }
                case NPC_SKULLS -> {
                    builder.updateEvent(
                            NpcSkullEvent.builder()
                                    .serverIndex(serverIndex)
                                    .skulled(packet.readBoolean())
                                    .build()
                    );
                }
                case NPC_WIELD -> {
                    builder.updateEvent(
                            NpcWieldEvent.builder()
                                    .serverIndex(serverIndex)
                                    .wieldAppearanceId1(Byte.toUnsignedInt(packet.readByte()))
                                    .wieldAppearanceId2(Byte.toUnsignedInt(packet.readByte()))
                                    .build()
                    );
                }
                case BUBBLE -> {
                    builder.updateEvent(
                            NpcBubbleEvent.builder()
                                    .serverIndex(serverIndex)
                                    .itemId(Short.toUnsignedInt(packet.readShort()))
                                    .build()
                    );
                }
            }
        }
        return builder.build();
    }
}
