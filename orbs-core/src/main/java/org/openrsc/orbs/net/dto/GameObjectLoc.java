package org.openrsc.orbs.net.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class GameObjectLoc {
    private final int direction;
    private final int id;
    private final int offsetX;
    private final int offsetY;
}