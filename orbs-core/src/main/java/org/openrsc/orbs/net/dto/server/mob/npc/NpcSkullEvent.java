package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.npc.model.NpcUpdateType;

@SuperBuilder
@Getter
public class NpcSkullEvent extends NpcUpdateEvent {
    private final NpcUpdateType type = NpcUpdateType.NPC_SKULLS;
    private final boolean skulled;
}
