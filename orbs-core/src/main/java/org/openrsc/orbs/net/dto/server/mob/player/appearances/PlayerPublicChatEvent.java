package org.openrsc.orbs.net.dto.server.mob.player.appearances;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerIcon;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerUpdateType;

@SuperBuilder
@Getter
public class PlayerPublicChatEvent extends PlayerUpdateEvent {
    private final PlayerUpdateType type = PlayerUpdateType.PUBLIC_CHAT;
    private final PlayerIcon icon;
    private final String message;
}
