package org.openrsc.orbs.net.connection;

import com.google.common.eventbus.EventBus;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.opcode.OpcodeManager;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.PacketDeserializer;
import org.openrsc.orbs.net.serializer.SerializerUtils;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import java.net.SocketException;
import java.text.MessageFormat;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Service
@Log4j2
@ChannelHandler.Sharable
@DependsOn("entityService")
public class ConnectionHandler extends ChannelInboundHandlerAdapter {
    public static final AttributeKey<EventBus> PLAYER_BUS_KEY = AttributeKey.valueOf("playerBus");
    public static final AttributeKey<Boolean> WAITING_FOR_LOGIN_KEY = AttributeKey.valueOf("waitingForLoginResponse");

    private final Executor executor;
    private final OpcodeManager opcodeManager;

    public ConnectionHandler(OpcodeManager opcodeManager) {
        this.opcodeManager = opcodeManager;
        this.executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        try {
            EventBus playerBus = ctx.channel().attr(PLAYER_BUS_KEY).get();
            executor.execute(() -> {
                if (msg instanceof Packet) {
                    Packet packet = (Packet) msg;
                    ServerOpcode opcode = ServerOpcode.fromOpcode(packet.getID());
                    if (opcode != null) {
                        Class<?> type = opcodeManager.getType(opcode);
                        if (type != null) {
                            PacketDeserializer<?> deserializer = SerializerUtils.getDeserializer(type);
                            Event event = (Event) deserializer.deserialize(packet, type);
                            if (playerBus != null && event != null) {
                                playerBus.post(event);
                            } else {
                                Optional.of(opcode).ifPresent(this::logUnhandledPacket);
                            }

                            if (packet.getBuffer().readableBytes() > 0) {
                                // Unread bytes indicative that the protocol was not mapped correctly
                                log.warn(
                                        MessageFormat.format("{0} bytes left unread from packet {1}",
                                                packet.getBuffer().readableBytes(),
                                                opcode.toString()
                                        )
                                );
                            }
                        } else {
                            Optional.of(opcode).ifPresent(this::logUnhandledPacket);
                        }
                        packet.getBuffer().release();
                    } else {
                        log.info("Unrecognized packet id: {}", packet.getID());
                    }
                } else {
                    log.error("Unexpected channel read: " + msg.toString());
                }
            });
        } catch (Exception e) {
            log.catching(e);
            e.printStackTrace();
        }
    }

    private void logUnhandledPacket(ServerOpcode serverOpcode) {
        log.info(MessageFormat.format("Unhandled packet {0}({1})", serverOpcode, serverOpcode.getOpCode()));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
        if (cause instanceof SocketException) {
            log.info("Disconnected");
        } else {
            log.catching(cause);
            cause.printStackTrace();
        }
    }
}
