package org.openrsc.orbs.net.dto.server.mob.npc;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.npc.model.NpcUpdateType;

@SuperBuilder
@Getter
public class NpcBubbleEvent extends NpcUpdateEvent {
    private final NpcUpdateType type = NpcUpdateType.BUBBLE;
    private final int itemId;
}