package org.openrsc.orbs.net.dto.client.trade;

import lombok.Builder;
import lombok.Getter;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;

@Builder
@Getter
public class TradeItem {
    @TypeOverride(DataType.SHORT)
    private final int itemId;
    private final int amount;
    @TypeOverride(DataType.SHORT)
    private final boolean noted;
}
