package org.openrsc.orbs.net.connection;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

@Component
public class ConnectionFactory {
    private final EventLoopGroup workerGroup;
    private final Bootstrap bootstrap;

    public ConnectionFactory(ConnectionHandler connectionHandler) {
        workerGroup = new NioEventLoopGroup();

        bootstrap = new Bootstrap(); // (1)
        bootstrap.group(workerGroup); // (2)
        bootstrap.channel(NioSocketChannel.class); // (3)
        bootstrap.option(ChannelOption.TCP_NODELAY, true);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, false);
        bootstrap.option(ChannelOption.SO_RCVBUF, 10000);
        bootstrap.option(ChannelOption.SO_SNDBUF, 10000);
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(final SocketChannel channel) {
                final ChannelPipeline pipeline = channel.pipeline();
                pipeline.addLast("decoder", new PacketDecoder());
                pipeline.addLast("encoder", new PacketEncoder());
                pipeline.addLast("handler", connectionHandler);
            }
        });
    }

    public ChannelFuture connect(String host, int port) throws InterruptedException {
        return bootstrap.connect(host, port).sync();
    }

    public ChannelFuture connect(InetAddress inetAddress, int port) throws InterruptedException {
        return bootstrap.connect(inetAddress, port).sync();
    }

    public void shutdown() {
        workerGroup.shutdownGracefully();
    }
}
