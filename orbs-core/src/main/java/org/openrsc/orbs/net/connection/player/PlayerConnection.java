package org.openrsc.orbs.net.connection.player;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import io.netty.channel.Channel;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.tuple.Pair;
import org.openrsc.orbs.net.builder.PacketBuilder;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.connection.player.script.ScriptState;
import org.openrsc.orbs.net.connection.player.script.ScriptThread;
import org.openrsc.orbs.net.connection.player.script.event.SleepStarted;
import org.openrsc.orbs.net.connection.player.script.event.SleepStopped;
import org.openrsc.orbs.net.connection.player.script.event.SleepWordProcessed;
import org.openrsc.orbs.net.dto.client.sleep.SleepWordAction;
import org.openrsc.orbs.net.dto.server.login.LoginEvent;
import org.openrsc.orbs.net.dto.server.sleep.SleepFatigueEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.opcode.ClientOpcode;
import org.openrsc.orbs.net.opcode.OpcodeManager;
import org.openrsc.orbs.net.serializer.SerializerUtils;
import org.openrsc.orbs.service.PlayerService;
import org.openrsc.orbs.stateful.AbstractReducer;
import org.openrsc.orbs.stateful.player.PlayerState;
import org.openrsc.orbs.stateful.player.model.LoginResponse;
import org.openrsc.orbs.util.ThreadUtils;
import org.openrsc.orbs.util.World;

import java.text.MessageFormat;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@Log4j2
public class PlayerConnection implements Runnable, AutoCloseable {
    public static final int HEARTBEAT_FREQUENCY_MS = 5000;
    public static final int ACTION_THROTTLE_MS = 50;
    private final ScriptAPI api;
    private final OpcodeManager opcodeManager;
    private final String username;
    private final String password;
    private final PlayerState playerState;
    private final World world;
    private final EventBus scriptEventBus;
    private final PlayerService playerService;
    private final Queue<Pair<Class<? extends Script>, Object>> scriptQueue = new ArrayBlockingQueue<>(15);
    private Channel channel;
    private Set<AbstractReducer> reducers;
    private LoginResponse loginResponse;
    @Getter
    private ReconnectStrategy reconnectStrategy = ReconnectStrategy.IMMEDIATE;
    @Getter(AccessLevel.PRIVATE)
    private long lastHeartbeat = System.currentTimeMillis();
    @Getter(AccessLevel.PRIVATE)
    private ScriptThread scriptThread;
    private long retryTimeStarted = System.currentTimeMillis();
    private long retryTime = System.currentTimeMillis();
    private boolean disconnected = false;
    @Getter
    private String errorMessage = null;
    @Getter
    private boolean fatalError = false;
    @Setter
    private boolean explicitlyRemoved = false;

    public PlayerConnection(
            String username,
            String password,
            PlayerState playerState,
            Channel channel,
            Set<AbstractReducer> reducers,
            OpcodeManager opcodeManager,
            World world,
            EventBus scriptEventBus,
            PlayerService playerService) {
        this.username = username;
        this.password = password;
        this.playerState = playerState;
        this.channel = channel;
        this.reducers = reducers;
        this.opcodeManager = opcodeManager;
        this.world = world;
        this.scriptEventBus = scriptEventBus;
        this.playerService = playerService;
        api = new ScriptAPI(scriptEventBus, playerService, playerState, this, world);
        scriptEventBus.register(this);
    }

    @Override
    public void run() {
        try {
            long maxWait = 5000;
            while (loginResponse == null) {
                maxWait -= 500;
                ThreadUtils.sleep(500);
                if (maxWait <= 0) {
                    reconnectStrategy = ReconnectStrategy.WAIT_ONE_MINUTE;
                    onDisconnect();
                    return;
                }
            }
            if (!loginResponse.isSuccess()) {
                switch (loginResponse) {
                    case ACCOUNT_LOGGEDIN:
                    case SERVER_TIMEOUT:
                    case LOGINSERVER_OFFLINE:
                    case LOGINSERVER_MISMATCH:
                        reconnectStrategy = ReconnectStrategy.WAIT_ONE_MINUTE;
                        break;
                    case LOGIN_ATTEMPTS_EXCEEDED:
                    case ACCOUNT_TEMP_DISABLED:
                        reconnectStrategy = ReconnectStrategy.WAIT_FIVE_MINUTES;
                        break;
                    case ACCOUNT_PERM_DISABLED:
                    case INVALID_CREDENTIALS:
                    case UNRECOGNIZED_LOGIN:
                        reconnectStrategy = ReconnectStrategy.NO_RETRY;
                        break;
                    default:
                        reconnectStrategy = ReconnectStrategy.IMMEDIATE;
                }
                errorMessage = loginResponse.getMessage();
                playerState.getMessageHistory().add(errorMessage);
            }
            if (scriptThread != null && !scriptThread.isAlive()) {
                startScript();
            }
            while (channel.isActive()) {
                handleHeartbeat();
                ThreadUtils.sleep(500);
                if (!scriptQueue.isEmpty()) {
                    if (scriptThread == null || scriptThread.getScriptState() == ScriptState.FINISHED) {
                        startNextScriptInQueue();
                    }
                }
            }
            onDisconnect();
        } catch (Exception ex) {
            log.catching(ex);
            ex.printStackTrace();
        }
    }

    public long getCurrentScriptTimeElapsed() {
        if (scriptThread != null) {
            return scriptThread.getTimeElapsedMs();
        }
        return 0;
    }

    public ScriptState getScriptState() {
        if (scriptThread != null) {
            return scriptThread.getScriptState();
        }
        return null;
    }

    private void startNextScriptInQueue() {
        try {
            final Pair<Class<? extends Script>, Object> scriptParamPair = scriptQueue.poll();
            Class<? extends Script> nextScriptClass = scriptParamPair.getLeft();
            Script nextScript = nextScriptClass.getConstructor(ScriptAPI.class).newInstance(api);
            Object params = scriptParamPair.getRight();
            nextScript.init(params);
            scriptThread = new ScriptThread(nextScript, api, scriptEventBus);
            startScript();
        } catch (Exception ex) {
            log.error("Unable to start script: ", ex);
        }
    }

    public void onDisconnect() {
        disconnected = true;
        switch (reconnectStrategy) {
            case NO_RETRY:
                fatalError = true;
                retryTime = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(5);
                break;
            case WAIT_ONE_MINUTE:
                retryTime = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(1);
                break;
            case WAIT_FIVE_MINUTES:
                retryTime = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5);
                break;
            case IMMEDIATE:
                retryTime = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(2);
                break;
        }
        int random = new Random().nextInt(100);
        retryTimeStarted = System.currentTimeMillis();
        ThreadUtils.sleep(retryTime - System.currentTimeMillis() + random);
        if (!explicitlyRemoved && reconnectStrategy != ReconnectStrategy.NO_RETRY) {
            playerService.login(username, getCurrentScript());
        } else if (reconnectStrategy == ReconnectStrategy.NO_RETRY) {
            playerService.logout(username);
        }
    }

    @Override
    @SneakyThrows
    public void close() {
        scriptEventBus.unregister(this);
        reducers.forEach(AbstractReducer::dispose);
        reducers = null;
        channel = null;
        System.gc();
    }

    public void queueScript(Class<? extends Script> script, Object params) {
        log.info(MessageFormat.format("{0}: Script queued", username));
        api.setStatus("Script Queued");
        scriptQueue.add(Pair.of(script, params));
    }

    public void startScript() {
        if (scriptThread != null) {
            if (scriptThread.isPaused()) {
                resumeScript();
            } else {
                log.info(MessageFormat.format("{0}: Script started", username));
                api.setStatus("Starting");
                scriptThread.start();
            }
        }
    }

    public void resumeScript() {
        if (scriptThread != null) {
            log.info(MessageFormat.format("{0}: Script resumed", username));
            scriptThread.resumeThread();
        }
    }

    public void pauseScript() {
        log.info(MessageFormat.format("{0}: Script paused", username));
        scriptThread.pauseThread();
    }

    public void sleepScript() {
        log.info(MessageFormat.format("{0}: Sleeping", username));
        scriptThread.sleep();
    }

    public void shutdownScript() {
        try {
            if (scriptThread != null) {
                log.info(MessageFormat.format("{0}: Script stopped", username));
                playerState.getMessageHistory().add("Requested script shutdown after current script iteration.");
                scriptThread.shutdown();
            }
        } catch (Exception ignored) {
        }
    }

    public Script<?> getCurrentScript() {
        if (scriptThread != null) {
            return scriptThread.getScript();
        }
        return null;
    }

    @Subscribe
    public void onSleepStartedEvent(SleepStarted event) {
        sleepScript();
    }

    @Subscribe
    public void onSleepFatigueEvent(SleepFatigueEvent event) {
        if (event.getFatigue() == 0) {
            trySendSleepWord();
        }
    }

    @Subscribe
    public void onSleepWordProcessed(SleepWordProcessed event) {
        log.info(MessageFormat.format("{0}: Sleepword processed", username));
        Integer sleepScreenFatigue = playerState.getSleepScreenFatigue().get();
        if (sleepScreenFatigue != null && sleepScreenFatigue == 0) {
            trySendSleepWord();
        }
    }

    @Subscribe
    public void onStopSleep(SleepStopped event) {
        log.info(MessageFormat.format("{0}: Sleep stopped", username));
        resumeScript();
    }

    @Subscribe
    public void onLoginResponse(LoginEvent event) {
        loginResponse = event.getLoginResponse();
    }

    public void trySendSleepWord() {
        AtomicReference<String> sleepWord = playerState.getSleepWord();
        if (sleepWord.get() != null) {
            SleepWordAction sleepWordAction = SleepWordAction.builder().sleepWord(sleepWord.get()).build();
            Packet packet = SerializerUtils.getSerializer(SleepWordAction.class).serialize(sleepWordAction);
            writePacket(packet);
            sleepWord.set(null);
        }
    }

    private void handleHeartbeat() {
        if (System.currentTimeMillis() - lastHeartbeat > HEARTBEAT_FREQUENCY_MS) {
            lastHeartbeat = System.currentTimeMillis();
            writePacket(
                    new PacketBuilder().setID(ClientOpcode.HEARTBEAT.getOpCode()).toPacket()
            );
        }
    }

    public void writePacket(Packet packet) {
        channel.writeAndFlush(packet);
    }
}
