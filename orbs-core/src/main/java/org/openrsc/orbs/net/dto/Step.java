package org.openrsc.orbs.net.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;

@AllArgsConstructor
@PacketSerializable
@Getter
@ToString
public class Step {
    @TypeOverride(DataType.BYTE)
    private final int offsetX;
    @TypeOverride(DataType.BYTE)
    private final int offsetY;
}
