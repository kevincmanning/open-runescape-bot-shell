package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.net.dto.server.trade.TradeOfferUpdateEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

public class TradeOfferUpdateEventDeserializer implements PacketDeserializer<TradeOfferUpdateEvent> {
    @Override
    public TradeOfferUpdateEvent deserialize(Packet packet, Class<?> object) {
        final TradeOfferUpdateEvent.TradeOfferUpdateEventBuilder<?, ?> builder = TradeOfferUpdateEvent.builder();
        builder.receivedItems(TradeOfferUtils.readTradeItems(packet));
        builder.offeredItems(TradeOfferUtils.readTradeItems(packet));
        return builder.build();
    }
}
