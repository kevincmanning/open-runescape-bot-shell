package org.openrsc.orbs.net.connection.player.script.event;

import lombok.Builder;
import lombok.Getter;
import org.openrsc.orbs.model.Skill;

@Builder(toBuilder = true)
@Getter
public class LevelUpEvent {
    private final Skill skill;
    private final int level;
}
