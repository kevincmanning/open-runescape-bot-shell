package org.openrsc.orbs.net.connection.player.script;

public class CommonIds {
    public static final int[] OBJ_CLAY = new int[]{114, 115};
    public static final int[] OBJ_TIN = new int[]{104, 105};
    public static final int[] OBJ_COPPER = new int[]{100, 101};
    public static final int[] OBJ_IRON = new int[]{102, 103};
    public static final int[] OBJ_SILVER = new int[]{195, 196};
    public static final int[] OBJ_COAL = new int[]{110, 111};
    public static final int[] OBJ_GOLD = new int[]{112, 113};
    public static final int[] OBJ_MITHRIL = new int[]{106, 107};
    public static final int[] OBJ_ADAMANTITE = new int[]{108, 109};
    public static final int[] OBJ_RUNITE = new int[]{210};
    public static final int[] OBJ_TREES = new int[]{0, 1};
    public static final int OBJ_FURANCE = 118;

    public static final int ITEM_COAL_ORE = 155;
    public static final int ITEM_SLEEPING_BAG = 1263;
    public static final int ITEM_BRONZE_PICKAXE = 156;
    public static final Integer[] PICKAXES = new Integer[]{
            ItemIds.BRONZE_PICKAXE.id(),
            ItemIds.IRON_PICKAXE.id(),
            ItemIds.STEEL_PICKAXE.id(),
            ItemIds.MITHRIL_PICKAXE.id(),
            ItemIds.ADAMANTITE_PICKAXE.id(),
            ItemIds.RUNE_PICKAXE.id()
    };

    public static final int[] NPC_BANKER_IDS = new int[]{95, 224, 268, 540, 617};

}
