package org.openrsc.orbs.net.dto.server.mob.player.appearances;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.dto.server.mob.player.appearances.model.PlayerUpdateType;

@SuperBuilder
@Getter
public abstract class PlayerUpdateEvent {
    private final int playerId;
    private final PlayerUpdateType type;
}
