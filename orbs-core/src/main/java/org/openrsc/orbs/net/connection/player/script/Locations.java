package org.openrsc.orbs.net.connection.player.script;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.util.World;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public enum Locations {
    AL_KHARID("Al Kharid", new Point(87, 695), true),
    ARDOUGNE("Ardougne", new Point(581, 572), true),
    BARB_VILLAGE("Barbarian Village", new Point(227, 506), false),
    CATHERBY("Catherby", new Point(440, 496), true),
    DRAYNOR("Draynor Village", new Point(219, 634), true),
    DRAYNOR_MANOR("Draynor Manor", new Point(214, 558), false),
    EDGEVILLE("Edgeville", new Point(216, 449), true),
    FALADOR_WEST("Falador West", new Point(328, 552), true),
    FALADOR_EAST("Falador East", new Point(286, 571), true),
    LUMBRIDGE("Lumbridge", new Point(121, 648), false),
    PORT_SARIM("Port Sarim", new Point(269, 639), false),
    SEERS_VILLAGE("Seers Village", new Point(501, 453), true),
    TAVERLY("Taverly", new Point(372, 490), false),
    VARROCK_WEST("Varrock West", new Point(151, 506), true),
    VARROCK_EAST("Varrock East", new Point(103, 510), true),
    YANILLE("Yanille", new Point(585, 752), true);

    private final String name;
    private final Point location;
    private final boolean isBank;

    public static Locations getNearest(Point location) {
        Point firstFloorLocation = new Point(location.getX(), location.getY() % World.DISTANCE_BETWEEN_FLOORS);
        return Arrays.stream(Locations.values())
                .min(Comparator.comparing(locations -> locations.getLocation().distance(firstFloorLocation)))
                .orElse(null);
    }

    public static Set<Locations> getNearestBanks(Point location) {
        Point firstFloorLocation = new Point(location.getX(), location.getY() % World.DISTANCE_BETWEEN_FLOORS);
        return Arrays.stream(Locations.values())
                .filter(Locations::isBank)
                .collect(Collectors.toCollection(
                        () -> new TreeSet<>(
                                Comparator.comparingInt(
                                        locations -> locations
                                                .getLocation()
                                                .distance(firstFloorLocation)
                                )
                        )
                ));
    }
}
