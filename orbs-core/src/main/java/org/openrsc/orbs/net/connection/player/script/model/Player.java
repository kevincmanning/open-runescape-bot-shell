package org.openrsc.orbs.net.connection.player.script.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.openrsc.orbs.model.Locatable;
import org.openrsc.orbs.model.Point;

import java.util.List;

@Data
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Player implements Locatable {
    private final Integer playerId;

    private final Point location;

    @EqualsAndHashCode.Include
    private final String username;
    private final List<Integer> wornItemAppearanceIds;
    private final Integer combatLevel;
    private final boolean skulled;
    private final String clanTag;
    private final boolean invisible;
    private final boolean invulnerable;
    private final Integer groupId;

    private final Integer currentHp;
    private final Integer maxHp;
}
