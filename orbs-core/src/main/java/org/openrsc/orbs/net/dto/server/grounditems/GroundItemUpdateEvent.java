package org.openrsc.orbs.net.dto.server.grounditems;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.ItemLoc;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.GroundItemUpdateEventDeserializer;

import java.util.List;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = GroundItemUpdateEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_GROUND_ITEM_HANDLER)
public class GroundItemUpdateEvent extends Event {
    private final List<ItemLoc> removedItems;
    private final List<ItemLoc> itemLocs;
}
