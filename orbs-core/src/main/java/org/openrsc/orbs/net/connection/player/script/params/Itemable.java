package org.openrsc.orbs.net.connection.player.script.params;

public interface Itemable {
    Integer getItemId();
}
