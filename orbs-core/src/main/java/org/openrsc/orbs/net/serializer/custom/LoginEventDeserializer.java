package org.openrsc.orbs.net.serializer.custom;

import org.openrsc.orbs.net.dto.server.login.LoginEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;
import org.openrsc.orbs.stateful.player.model.LoginResponse;

public class LoginEventDeserializer implements PacketDeserializer<LoginEvent> {
    @Override
    public LoginEvent deserialize(Packet packet, Class<?> object) {
        return LoginEvent.builder()
                .loginResponse(
                        LoginResponse.fromResponseCode(packet.readByte())
                ).build();
    }
}
