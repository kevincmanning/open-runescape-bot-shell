package org.openrsc.orbs.net.connection.player.script.params;

import lombok.Builder;
import lombok.Getter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;

import java.util.List;

@SuperBuilder
@Getter
public abstract class ScriptParam {
    private final String key;
    private final String label;
    @Builder.Default
    private final boolean multiple = false;

    @Singular
    // Controls whether the parameter is applicable
    private final List<EnabledCondition> enabledConditions;

    public abstract ScriptParamType getType();
}
