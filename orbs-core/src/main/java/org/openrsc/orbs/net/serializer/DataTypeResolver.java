package org.openrsc.orbs.net.serializer;

import lombok.NoArgsConstructor;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@NoArgsConstructor
public class DataTypeResolver {
    private static final Map<String, DataType> primitiveMap;
    private static final Map<Class<?>, DataType> resolvedTypes;

    static {
        primitiveMap = Map.of(
                "byte[]", DataType.BYTE_ARRAY,
                "boolean", DataType.BYTE,
                "byte", DataType.BYTE,
                "short", DataType.SHORT,
                "long", DataType.LONG,
                "int", DataType.INT
        );

        resolvedTypes = new ConcurrentHashMap<>();
        resolvedTypes.put(Short.class, DataType.SHORT);
        resolvedTypes.put(Integer.class, DataType.INT);
        resolvedTypes.put(Long.class, DataType.LONG);
        resolvedTypes.put(Boolean.class, DataType.BYTE);
        resolvedTypes.put(Byte.class, DataType.BYTE);
        resolvedTypes.put(String.class, DataType.STRING);
        resolvedTypes.put(List.class, DataType.LIST);

    }

    public DataTypeConverter getType(Class<?> type) {
        DataType resolvedType = null;
        String typeName = type.getSimpleName();
        resolvedType = primitiveMap.get(typeName);

        if (type.isPrimitive() && resolvedType == null) {
            throw new RuntimeException(
                    MessageFormat.format(
                            "Unsupported primitive type in {0}: {1}",
                            type.getSimpleName(),
                            typeName
                    )
            );
        }
        if (resolvedType == null) {
            resolvedType = resolvedTypes.computeIfAbsent(type, key -> DataType.OBJECT);
        }
        return new DataTypeConverter(type, resolvedType);
    }

    public DataTypeConverter getType(Class<?> type, TypeOverride explicitOverride) {
        return new DataTypeConverter(type, explicitOverride.value());
    }
}
