package org.openrsc.orbs.net.connection.player.script.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class QuestMessage {
    private final String message;
}
