package org.openrsc.orbs.net.dto.server.mob.player.appearances.model;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import lombok.Getter;

import java.util.Arrays;
import java.util.Collection;

@Getter
public enum PlayerIcon {
    ADMIN(0x0100FF00, 0x02FFFFFF),
    MOD(0x010000FF, 0x03FFFFFF),
    DEV(0x01FF0000),
    EVENT(0x014D33BD),
    PLAYER_MOD(0x03FFFFFF);

    private static final Multimap<Integer, PlayerIcon> byId = HashMultimap.create();

    static {
        Arrays.stream(PlayerIcon.values()).forEach(icon -> {
            Arrays.stream(icon.getIconId()).forEach(id -> byId.put(id, icon));
        });
    }

    private final int[] iconId;

    PlayerIcon(int... iconId) {
        this.iconId = iconId;
    }

    public static Collection<PlayerIcon> byId(int iconId) {
        return byId.get(iconId);
    }

    public static PlayerIcon firstById(int iconId) {
        return PlayerIcon.byId(iconId)
                .stream()
                .findFirst()
                .orElse(null);
    }
}
