package org.openrsc.orbs.net.dto.client.walk;

import lombok.Getter;
import lombok.Singular;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.dto.Point;
import org.openrsc.orbs.net.dto.Step;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

import java.util.List;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterActionOpcode(ClientOpcode.WALK_TO_ENTITY)
public class WalkToEntityAction extends Action {
    private final Point location;
    @Singular
    private final List<Step> steps;
}
