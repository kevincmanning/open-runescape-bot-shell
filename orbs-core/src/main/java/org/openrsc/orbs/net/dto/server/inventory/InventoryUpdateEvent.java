package org.openrsc.orbs.net.dto.server.inventory;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.model.InventoryItem;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.InventoryUpdateEventDeserializer;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = InventoryUpdateEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_INVENTORY_UPDATEITEM)
public class InventoryUpdateEvent extends Event {
    private final int slotId;
    private InventoryItem item;
}