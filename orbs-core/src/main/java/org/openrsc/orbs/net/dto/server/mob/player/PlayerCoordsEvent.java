package org.openrsc.orbs.net.dto.server.mob.player;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.Point;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.PlayerCoordsEventDeserializer;

import java.util.LinkedHashMap;
import java.util.Set;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = PlayerCoordsEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_PLAYER_COORDS)
public class PlayerCoordsEvent extends Event {
    private final Point currentPlayerLocation;
    private final boolean inCombat;
    private final Set<Integer> removedPlayers;
    private final LinkedHashMap<Integer, Point> playerStatusUpdates;
    private final LinkedHashMap<Integer, Boolean> playersInCombat;
}
