package org.openrsc.orbs.net.dto.client.equip;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterActionOpcode(ClientOpcode.ITEM_UNEQUIP_FROM_EQUIPMENT)
public class UnEquipFromEquipmentAction extends Action implements EquipAction {
    @TypeOverride(DataType.BYTE)
    private final int slotIndex;
}
