package org.openrsc.orbs.net.dto.client.equip;

public interface EquipAction {
    int getSlotIndex();
}
