package org.openrsc.orbs.net.serializer;

import org.openrsc.orbs.net.model.Packet;

public interface PacketSerializer<T> {
    Packet serialize(T object);
}
