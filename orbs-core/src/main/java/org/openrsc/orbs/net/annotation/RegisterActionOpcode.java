package org.openrsc.orbs.net.annotation;

import org.openrsc.orbs.net.opcode.ClientOpcode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Repeatable(RegisterActionOpcodes.class)
public @interface RegisterActionOpcode {
    ClientOpcode value();
}
