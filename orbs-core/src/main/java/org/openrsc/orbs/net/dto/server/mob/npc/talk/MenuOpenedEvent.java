package org.openrsc.orbs.net.dto.server.mob.npc.talk;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

import java.util.List;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterEventOpcode(ServerOpcode.SEND_OPTIONS_MENU_OPEN)
public class MenuOpenedEvent extends Event {
    private final List<String> options;
}
