package org.openrsc.orbs.net.dto.server.bank;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;
import org.openrsc.orbs.net.serializer.custom.BankOpenEventDeserializer;

import java.util.List;

@SuperBuilder
@Getter
@PacketSerializable(deserializer = BankOpenEventDeserializer.class)
@RegisterEventOpcode(ServerOpcode.SEND_BANK_OPEN)
public class BankOpenEvent extends Event {
    @TypeOverride(DataType.SHORT)
    private final int itemsStoredSize;
    @TypeOverride(DataType.SHORT)
    private final int maxBankSize;
    private final List<BankSlot> bankSlot;
}