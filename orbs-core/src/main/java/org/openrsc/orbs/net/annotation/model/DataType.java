package org.openrsc.orbs.net.annotation.model;

public enum DataType {
    BYTE_ARRAY,
    BYTE,
    UNSIGNED_SHORT,
    SHORT,
    INT,
    LONG,
    ENCRYPTED_STRING,
    STRING,
    OBJECT,
    LIST
}
