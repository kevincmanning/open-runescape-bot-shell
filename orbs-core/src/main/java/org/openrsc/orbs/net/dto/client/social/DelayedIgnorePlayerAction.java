package org.openrsc.orbs.net.dto.client.social;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterActionOpcode;
import org.openrsc.orbs.net.dto.client.Action;
import org.openrsc.orbs.net.opcode.ClientOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterActionOpcode(ClientOpcode.SOCIAL_ADD_DELAYED_IGNORE)
public class DelayedIgnorePlayerAction extends Action implements SocialAction {
    private final String player;
}
