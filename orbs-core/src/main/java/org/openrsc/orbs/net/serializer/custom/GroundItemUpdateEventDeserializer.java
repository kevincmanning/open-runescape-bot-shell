package org.openrsc.orbs.net.serializer.custom;

import io.netty.buffer.ByteBuf;
import org.openrsc.orbs.Main;
import org.openrsc.orbs.net.dto.ItemLoc;
import org.openrsc.orbs.net.dto.server.grounditems.GroundItemUpdateEvent;
import org.openrsc.orbs.net.model.Packet;
import org.openrsc.orbs.net.serializer.PacketDeserializer;

import java.util.ArrayList;
import java.util.List;

public class GroundItemUpdateEventDeserializer implements PacketDeserializer<GroundItemUpdateEvent> {
    @Override
    public GroundItemUpdateEvent deserialize(Packet packet, Class<?> type) {
        List<ItemLoc> itemLocs = new ArrayList<>();
        List<ItemLoc> removedItems = new ArrayList<>();

        ByteBuf payload = packet.getBuffer();
        while (payload.readableBytes() > 0) {
            payload.markReaderIndex();
            byte shouldReadRespawn = payload.readByte();
            payload.resetReaderIndex();

            short itemId = -1;
            if (shouldReadRespawn == -1) {
                payload.readByte();
            } else {
                itemId = payload.readShort();
            }
            int offsetX = payload.readByte();
            int offsetY = payload.readByte();
            boolean noted = false;
            if (Main.SERVER_CONFIGURATION != null && Main.SERVER_CONFIGURATION.isWantBankNotes()) {
                noted = payload.readBoolean();
            }

            if (itemId < -1) {
                // Item should be removed
                itemId -= 32768;
                removedItems.add(new ItemLoc(noted, itemId, -1, offsetX, offsetY));
            } else {
                itemLocs.add(
                        new ItemLoc(noted, itemId, -1, offsetX, offsetY)
                );
            }
        }
        return GroundItemUpdateEvent.builder().itemLocs(itemLocs).removedItems(removedItems).build();
    }
}
