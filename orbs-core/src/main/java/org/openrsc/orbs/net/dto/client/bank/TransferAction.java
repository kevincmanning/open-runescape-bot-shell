package org.openrsc.orbs.net.dto.client.bank;


public interface TransferAction {
    int getCatalogId();

    int getAmount();
}
