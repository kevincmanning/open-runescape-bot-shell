package org.openrsc.orbs.net.dto.server.config;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.openrsc.orbs.net.annotation.PacketSerializable;
import org.openrsc.orbs.net.annotation.RegisterEventOpcode;
import org.openrsc.orbs.net.annotation.TypeOverride;
import org.openrsc.orbs.net.annotation.model.DataType;
import org.openrsc.orbs.net.dto.server.Event;
import org.openrsc.orbs.net.opcode.ServerOpcode;

@SuperBuilder
@Getter
@PacketSerializable
@RegisterEventOpcode(ServerOpcode.SEND_SERVER_CONFIGS)
public class ServerConfigurationEvent extends Event {
    private final String serverName;     // 1
    private final String serverNameWelcome;     // 2
    @TypeOverride(DataType.BYTE)
    private final int playerLevelLimit; // 3
    private final boolean spawnAuctionNpcs; // 4
    private final boolean spawnIronManNpcs; // 5
    private final boolean spawnFloatingNametags; // 6
    private final boolean wantClans; // 7
    private final boolean wantKillFeed; // 8
    private final boolean fogToggle; // 9
    private final boolean groundItemToggle; // 10
    private final boolean autoMessageSwitchToggle; // 11
    private final boolean batchProgression; // 12
    private final boolean sideMenuToggle; // 13
    private final boolean inventoryCountToggle; // 14
    private final boolean zoomViewToggle; // 15
    private final boolean menuCombatStyleToggle; // 16
    private final boolean fightModeSelectorToggle; // 17
    private final boolean experienceCounterToggle; // 18
    private final boolean experienceDropsToggle; // 19
    private final boolean itemsOnDeathMenu; // 20
    private final boolean showRoofToggle; // 21
    private final boolean wantHideIP; // 22
    private final boolean wantRemember; // 23
    private final boolean wantGlobalChat; // 24
    private final boolean wantSkillMenus; // 25
    private final boolean wantQuestMenus; // 26
    private final boolean wantExperienceElixirs; // 27
    private final boolean wantKeyboardShortcuts; // 28
    private final boolean wantCustomBanks; // 29
    private final boolean wantBankPins; // 30
    private final boolean wantBankNotes; // 31
    private final boolean wantCertDeposit; // 32
    private final boolean customFiremaking; // 33
    private final boolean wantDropX; // 34
    private final boolean wantExpInfo; // 35
    private final boolean wantWoodcuttingGuild; // 36
    private final boolean wantDecanting; // 37
    private final boolean wantCerterBankExchange; // 38
    private final boolean wantCustomRankDisplay; // 39
    private final boolean rightClickBank; // 40
    private final boolean fixOverheadChat; // 41
    private final String welcomeText;     // 42
    private final boolean memberWorld; // 43
    private final boolean displayLogoSprite; // 44
    private final String logoSpriteId;     // 45
    @TypeOverride(DataType.BYTE)
    private final int FPS; // 46
    private final boolean wantEmail; // 47
    private final boolean wantRegistrationLimit; // 48
    private final boolean allowResize; // 49
    private final boolean lenientContactDetails; // 50
    private final boolean wantFatigue; // 51
    private final boolean wantCustomSprites; // 52
    private final boolean playerCommands; // 53
    private final boolean wantPets; // 54
    @TypeOverride(DataType.BYTE)
    private final int maxWalkingSpeed; // 55
    private final boolean showUnidentifiedHerbNames; // 56
    private final boolean wantQuestStartedIndicator; // 57
    private final boolean fishingSpotsDepletable; // 58
    private final boolean improvedItemObjectNames; // 59
    private final boolean wantRunecraft; //60
    private final boolean wantCustomLandscape; //61
    private final boolean wantEquipmentTab; //62
    private final boolean wantBankPresets; //63
    private final boolean wantParties; //64
    private final boolean miningRocksExtended; //65
    @TypeOverride(DataType.BYTE)
    private final int stepsPerFrame; //66
    private final boolean wantLeftclickWebs; //67
    private final boolean npcLogKillMessages; //68
    private final boolean wantCustomUi; //69
    private final boolean wantGlobalFriend; //70
    private final byte characterCreationMode; //71
    private final byte skillingExpRate; //72
    private final boolean wantHarvesting; // 73
    private final boolean hideLoginBoxToggle; // 74
    private final boolean wantGlobalFriendAgain; // 75
    private final boolean rightClickTrade; // 76
    private final boolean nothingReuseMe; // 77
    private final boolean wantExtendedCatsBehavior; // 78
    private final boolean wantCertAsNotes; // 79
    private final boolean wantOpenpkPoints; // 80
    private final byte openpkPointsToGpRatio; // 81
}
