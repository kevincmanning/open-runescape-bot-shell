package org.openrsc.orbs.model.data;

/**
 * The abstract class EntityDef implements methods for return values which are
 * shared between entities.
 */
public abstract class EntityDef {
    public String name;
    public String description;
    public int id;


    EntityDef(String name, String description, int id) {
        this.name = name;
        this.description = description;
        this.id = id;
    }

    EntityDef(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}