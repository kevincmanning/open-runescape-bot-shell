package org.openrsc.orbs.model.persisted;

import lombok.Builder;
import lombok.Getter;

import java.util.Set;

@Builder
@Getter
public class GroupDefinition {
    private final String name;
    private final Set<PlayerDefinition> players;
}
