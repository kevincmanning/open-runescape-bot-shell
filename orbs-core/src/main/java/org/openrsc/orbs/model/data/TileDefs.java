package org.openrsc.orbs.model.data;

import java.util.ArrayList;

public class TileDefs {
    public static final ArrayList<TileDef> tiles = new ArrayList<>();

    static {
        loadTileDefinitions();
    }

    private static void loadTileDefinitions() {
        tiles.add(new TileDef(-16913, 1, 0));
        tiles.add(new TileDef(1, 3, 1));
        tiles.add(new TileDef(3, 2, 0));
        tiles.add(new TileDef(3, 4, 0));
        tiles.add(new TileDef(-16913, 2, 0));
        tiles.add(new TileDef(-27685, 2, 0));
        tiles.add(new TileDef(25, 3, 1));
        tiles.add(new TileDef(12345678, 5, 1));
        tiles.add(new TileDef(-26426, 1, 1));
        tiles.add(new TileDef(-1, 5, 1));
        tiles.add(new TileDef(31, 3, 1));
        tiles.add(new TileDef(3, 4, 0));
        tiles.add(new TileDef(-4534, 2, 0));
        tiles.add(new TileDef(32, 2, 0));
        tiles.add(new TileDef(-9225, 2, 0));
        tiles.add(new TileDef(-3172, 2, 0));
        tiles.add(new TileDef(15, 2, 0));
        tiles.add(new TileDef(-2, 2, 0));
        tiles.add(new TileDef(-1, 3, 1));
        tiles.add(new TileDef(-2, 4, 0));
        tiles.add(new TileDef(-2, 4, 1));
        tiles.add(new TileDef(-2, 0, 0));
        tiles.add(new TileDef(-17793, 2, 0));
        tiles.add(new TileDef(-14594, 1, 1));
        tiles.add(new TileDef(1, 3, 0));
    }
}
