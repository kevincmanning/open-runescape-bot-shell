package org.openrsc.orbs.model;

import java.util.function.Function;

public interface TileTraversalProvider extends Function<Point, TileTraversal> {
}
