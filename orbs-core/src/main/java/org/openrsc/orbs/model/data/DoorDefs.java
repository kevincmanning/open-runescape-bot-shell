package org.openrsc.orbs.model.data;

import java.util.ArrayList;

public class DoorDefs {
    public static final ArrayList<DoorDef> doors = new ArrayList<>();

    static {
        loadDoorDefinitions();
    }

    private static void loadDoorDefinitions() {
        int i = 0;
        doors.add(new DoorDef("Wall", "", "WalkTo", "Examine", 1, 0, 192, 2, 2,
                i++));
        doors.add(new DoorDef("Doorframe", "", "WalkTo", "Close", 0, 1, 192, 4,
                4, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Window", "", "WalkTo", "Examine", 1, 0, 192, 5,
                5, i++));
        doors.add(new DoorDef("Fence", "", "WalkTo", "Examine", 1, 0, 192, 10,
                10, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "Examine", 1, 0, 192,
                12, 12, i++));
        doors.add(new DoorDef("Stained glass window", "", "WalkTo", "Examine",
                1, 0, 192, 18, 18, i++));
        doors.add(new DoorDef("Highwall", "", "WalkTo", "Examine", 1, 0, 275,
                2, 2, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 275, 0, 0, i++));
        doors.add(new DoorDef("Doorframe", "", "WalkTo", "Close", 0, 1, 275, 4,
                4, i++));
        doors.add(new DoorDef("battlement", "", "WalkTo", "Examine", 1, 0, 70,
                2, 2, i++));
        doors.add(new DoorDef("Doorframe", "", "WalkTo", "Examine", 1, 0, 192,
                4, 4, i++));
        doors.add(new DoorDef("snowwall", "", "WalkTo", "Examine", 1, 0, 192,
                -31711, -31711, i++));
        doors.add(new DoorDef("arrowslit", "", "WalkTo", "Examine", 1, 0, 192,
                7, 7, i++));
        doors.add(new DoorDef("timberwall", "", "WalkTo", "Examine", 1, 0, 192,
                21, 21, i++));
        doors.add(new DoorDef("timberwindow", "", "WalkTo", "Examine", 1, 0,
                192, 22, 22, i++));
        doors.add(new DoorDef("blank", "", "WalkTo", "Examine", 0, 0, 192,
                12345678, 12345678, i++));
        doors.add(new DoorDef("highblank", "", "WalkTo", "Examine", 0, 0, 275,
                12345678, 12345678, i++));
        doors.add(new DoorDef("mossybricks", "", "WalkTo", "Examine", 1, 0,
                192, 23, 23, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Odd looking wall",
                "This wall doesn't look quite right", "Push", "Examine", 1, 1,
                192, 2, 2, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("web", "A spider's web", "WalkTo", "Examine", 1,
                1, 192, 26, 26, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Window", "", "WalkTo", "Examine", 1, 0, 192, 27,
                27, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Crumbled", "", "WalkTo", "Examine", 1, 0, 192,
                28, 28, i++));
        doors.add(new DoorDef("Cavern", "", "WalkTo", "Examine", 1, 0, 192, 29,
                29, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("cavern2", "", "WalkTo", "Examine", 1, 0, 192,
                30, 30, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Wall", "", "WalkTo", "Examine", 1, 0, 192, 3, 3,
                i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Strange looking wall",
                "This wall doesn't look quite right", "Push", "Examine", 1, 1,
                192, 29, 29, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("memberrailings", "", "WalkTo", "Examine", 1, 0,
                192, 12, 12, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Magic Door", "The door is shut", "Open",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Strange Panel",
                "This wall doesn't look quite right", "Push", "Examine", 1, 1,
                192, 21, 21, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("blockblank", "", "WalkTo", "Examine", 1, 0, 192,
                12345678, 12345678, i++));
        doors.add(new DoorDef("unusual looking wall",
                "This wall doesn't look quite right", "Push", "Examine", 1, 1,
                192, 2, 2, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Pick lock",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Pick lock",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Pick lock",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Pick lock",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Pick lock",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Pick Lock",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Pick lock",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Fence with loose pannels",
                "I wonder if I could get through this", "push", "Examine", 1,
                1, 192, 10, 10, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("rat cage", "The rat's have damaged the bars",
                "search", "Examine", 1, 1, 192, 12, 12, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("arrowslit", "", "WalkTo", "Examine", 1, 0, 192,
                44, 44, i++));
        doors.add(new DoorDef("solidblank", "", "WalkTo", "Examine", 1, 0, 192,
                12345678, 12345678, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("loose panel", "The panel has worn with age",
                "break", "Examine", 1, 1, 192, 3, 3, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("plankswindow", "", "WalkTo", "Examine", 1, 0,
                192, 45, 45, i++));
        doors.add(new DoorDef("Low Fence", "", "WalkTo", "Examine", 1, 0, 96,
                10, 10, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Cooking pot", "Smells good!", "WalkTo",
                "Examine", 1, 1, 96, 10, 10, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("plankstimber", "", "WalkTo", "Examine", 1, 0,
                192, 46, 46, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("magic portal", "", "enter", "Examine", 1, 1,
                192, 17, 17, i++));
        doors.add(new DoorDef("magic portal", "", "enter", "Examine", 1, 1,
                192, 17, 17, i++));
        doors.add(new DoorDef("magic portal", "", "enter", "Examine", 1, 1,
                192, 17, 17, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Cavern wall",
                "It looks as if it is covered in some fungus.", "WalkTo",
                "search", 1, 1, 192, 29, 29, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "the door is shut", "walk through",
                "Examine", 1, 1, 192, 3, 3, i++));
        doors.add(new DoorDef("Door", "The door is shut", "walk through",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "walk through",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "walk through",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "walk through",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "walk through",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "walk through",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "walk through",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Pick Lock",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Low wall", "a low wall", "jump", "Examine", 1,
                1, 70, 2, 2, i++));
        doors.add(new DoorDef("Low wall", "a low wall", "jump", "Examine", 1,
                1, 70, 2, 2, i++));
        doors.add(new DoorDef("Blacksmiths Door", "The door is shut", "Open",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("railings", "", "pick lock", "Examine", 1, 1,
                192, 12, 12, i++));
        doors.add(new DoorDef("railings", "", "pick lock", "Examine", 1, 1,
                192, 12, 12, i++));
        doors.add(new DoorDef("railings", "", "pick lock", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "pick lock", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "pick lock", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "look through", 1, 1,
                192, 12, 12, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "knock on",
                1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Doorframe", "", "WalkTo", "Close", 1, 1, 192, 4,
                4, i++));
        doors.add(new DoorDef("Tent", "", "WalkTo", "Examine", 1, 0, 192, 36,
                36, i++));
        doors.add(new DoorDef("Jail Door", "The door is shut", "Open",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Jail Door", "The door is shut", "Open",
                "Examine", 1, 1, 192, 0, 0, i++));
        doors.add(new DoorDef("Window", "A barred window", "WalkTo", "Search",
                1, 1, 192, 27, 27, i++));
        doors.add(new DoorDef("magic portal",
                "A magical barrier shimmers with power", "WalkTo", "Examine",
                1, 1, 192, 17, 17, i++));
        doors.add(new DoorDef("Jail Door", "A solid iron gate", "Open",
                "Examine", 1, 1, 192, 12, 12, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("Cave exit", "The way out", "Leave", "Examine",
                0, 1, 192, 26, 26, i++));
        doors.add(new DoorDef("Cave exit", "The way out", "Leave", "Examine",
                0, 1, 192, 26, 26, i++));
        doors.add(new DoorDef("Cave exit", "The way out", "Leave", "Examine",
                0, 1, 192, 26, 26, i++));
        doors.add(new DoorDef("Cave exit", "The way out", "Leave", "Examine",
                0, 1, 192, 26, 26, i++));
        doors.add(new DoorDef("Cave exit", "The way out", "Leave", "Examine",
                0, 1, 192, 26, 26, i++));
        doors.add(new DoorDef("Cave exit", "The way out", "Leave", "Examine",
                0, 1, 192, 26, 26, i++));
        doors.add(new DoorDef("railings", "", "WalkTo", "search", 1, 1, 192,
                12, 12, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("battlement", "This is blocking your path",
                "Climb-over", "Examine", 1, 1, 70, 2, 2, i++));
        doors.add(new DoorDef("Tent Door", "An entrance into the tent",
                "Go through", "Examine", 1, 1, 192, 50, 50, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
        doors.add(new DoorDef("Tent Door", "An entrance into the tent",
                "Go through", "Examine", 1, 1, 192, 50, 50, i++));
        doors.add(new DoorDef("Low Fence", "A damaged wooden fence", "search",
                "Examine", 1, 1, 96, 10, 10, i++));
        doors.add(new DoorDef("Sturdy Iron Gate", "A solid iron gate", "Open",
                "Examine", 1, 1, 192, 12, 12, i++));
        doors.add(new DoorDef("battlement", "this low wall blocks your path",
                "climb over", "Examine", 1, 1, 70, 2, 2, i++));
        doors.add(new DoorDef("Water", "My waterfall boundary!", "WalkTo",
                "Examine", 1, 0, 192, 25, 25, i++));
        doors.add(new DoorDef("Wheat", "Test Boundary!", "WalkTo", "Examine",
                1, 0, 192, 24, 24, i++));
        doors.add(new DoorDef("Jungle", "Thick inpenetrable jungle", "Chop",
                "Examine", 1, 1, 192, 8, 8, i++));
        doors.add(new DoorDef("Window",
                "you can see a vicious looking guard dog right outside",
                "Investigate", "Examine", 1, 1, 192, 5, 5, i++));
        doors.add(new DoorDef("Rut",
                "Looks like a small rut carved into the ground.", "WalkTo",
                "Search", 1, 0, 96, 51, 51, i++));
        doors.add(new DoorDef("Crumbled Cavern 1", "", "WalkTo", "Examine", 0,
                0, 192, 52, 52, i++));
        doors.add(new DoorDef("Crumbled Cavern 2", "", "WalkTo", "Examine", 0,
                0, 192, 53, 53, i++));
        doors.add(new DoorDef("cavernhole", "", "WalkTo", "Examine", 1, 0, 192,
                54, 54, i++));
        doors.add(new DoorDef("flamewall",
                "A supernatural fire of incredible intensity", "Touch",
                "Investigate", 1, 1, 192, 54, 54, i++));
        doors.add(new DoorDef("Ruined wall",
                "Some ancient wall structure - it doesn't look too high.",
                "WalkTo", "Jump", 1, 1, 192, 28, 28, i++));
        doors.add(new DoorDef(
                "Ancient Wall",
                "An ancient - slightly higher wall with some strange markings on it",
                "Use", "Search", 1, 1, 275, 2, 2, i++));
        doors.add(new DoorDef("Door", "The door is shut", "Open", "Examine", 1,
                1, 192, 0, 0, i++));
    }
}
