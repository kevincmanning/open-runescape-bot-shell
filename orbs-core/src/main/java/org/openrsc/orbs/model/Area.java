package org.openrsc.orbs.model;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class Area {
    private final Point a;
    private final Point b;

    public static Area fromPoint(Point point, int dist) {
        return Area.builder()
                .a(new Point(point.getX() - dist, point.getY() - dist))
                .b(new Point(point.getX() + dist, point.getY() + dist))
                .build();
    }

    public boolean contains(Point point) {
        int minX = Math.min(a.getX(), b.getX());
        int maxX = Math.max(a.getX(), b.getX());
        int minY = Math.min(a.getY(), b.getY());
        int maxY = Math.max(a.getY(), b.getY());

        return minX <= point.getX()
                && point.getX() <= maxX
                && minY <= point.getY()
                && point.getY() <= maxY;
    }
}
