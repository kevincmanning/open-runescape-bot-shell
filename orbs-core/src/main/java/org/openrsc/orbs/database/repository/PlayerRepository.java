package org.openrsc.orbs.database.repository;

import org.openrsc.orbs.database.entity.PlayerEntity;
import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<PlayerEntity, String> {
}
