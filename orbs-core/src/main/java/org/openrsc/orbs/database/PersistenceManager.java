package org.openrsc.orbs.database;

import org.openrsc.orbs.database.entity.PlayerEntity;
import org.openrsc.orbs.database.entity.PlayerGroupEntity;
import org.openrsc.orbs.database.repository.GroupRepository;
import org.openrsc.orbs.database.repository.PlayerRepository;
import org.openrsc.orbs.model.persisted.GroupDefinition;
import org.openrsc.orbs.model.persisted.PlayerDefinition;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PersistenceManager {
    private final PlayerRepository playerRepository;
    private final GroupRepository groupRepository;

    public PersistenceManager(
            PlayerRepository playerRepository,
            GroupRepository groupRepository
    ) {
        this.playerRepository = playerRepository;
        this.groupRepository = groupRepository;
    }

    public PlayerDefinition addPlayer(PlayerDefinition playerDefinition) {
        boolean exists = playerRepository.findById(playerDefinition.getUsername()).isPresent();
        if (!exists) {
            PlayerEntity player = new PlayerEntity();
            player.setUsername(playerDefinition.getUsername());
            player.setPassword(playerDefinition.getPassword());
            player.setActive(playerDefinition.isActive());
            playerRepository.save(player);
            return PlayerEntity.toPlayerDefinition(player);
        }
        return null;
    }

    public PlayerDefinition savePlayer(PlayerDefinition playerDefinition) {
        final Optional<PlayerEntity> byId = playerRepository.findById(playerDefinition.getUsername());
        if (byId.isPresent()) {
            PlayerEntity player = byId.get();
            player.setActive(playerDefinition.isActive());
            playerRepository.save(player);
            return PlayerEntity.toPlayerDefinition(player);
        }
        return addPlayer(playerDefinition);
    }

    public void addPlayerToGroup(String groupName, String username) {
        groupRepository.findById(groupName).ifPresent(group ->
                playerRepository.findById(username).ifPresent(user -> {
                    group.getMembers().add(user);
                    groupRepository.save(group);
                })
        );
    }

    public Set<PlayerDefinition> getAllPlayers() {
        return StreamSupport.stream(playerRepository.findAll().spliterator(), false)
                .map(PlayerEntity::toPlayerDefinition)
                .collect(Collectors.toCollection(
                        () -> new TreeSet<>(Comparator.comparing(PlayerDefinition::getUsername))
                ));
    }

    public Set<GroupDefinition> getAllGroups() {
        return StreamSupport.stream(groupRepository.findAll().spliterator(), false)
                .map(PlayerGroupEntity::toGroupDefinition)
                .collect(Collectors.toSet());
    }

    public GroupDefinition getGroupById(String name) {
        final PlayerGroupEntity playerGroupEntity = groupRepository.findById(name).orElse(null);
        if (playerGroupEntity != null) {
            return PlayerGroupEntity.toGroupDefinition(playerGroupEntity);
        }
        return null;
    }

    public PlayerDefinition getPlayerById(String username) {
        final PlayerEntity playerEntity = playerRepository.findById(username).orElse(null);
        if (playerEntity != null) {
            return PlayerEntity.toPlayerDefinition(playerEntity);
        }
        return null;
    }

    public synchronized void unregisterPlayer(String username) {
        playerRepository.deleteById(username);
    }
}
