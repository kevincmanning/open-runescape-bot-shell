package org.openrsc.orbs.database.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.openrsc.orbs.model.persisted.PlayerDefinition;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "Player")
@Getter
@Setter
@FieldNameConstants
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PlayerEntity {
    @Id
    @EqualsAndHashCode.Include
    private String username;

    private String password;

    private Boolean active;

    @ManyToMany(targetEntity = PlayerGroupEntity.class, cascade = {CascadeType.ALL})
    @JoinTable(name = "PlayerGroupPlayer",
            joinColumns = {@JoinColumn(name = "groupName")},
            inverseJoinColumns = {@JoinColumn(name = "username")})
    private List<PlayerGroupEntity> groups;

    public static PlayerDefinition toPlayerDefinition(PlayerEntity entity) {
        return PlayerDefinition.builder()
                .username(entity.getUsername())
                .password(entity.getPassword())
                .active(entity.getActive())
                .build();
    }
}
