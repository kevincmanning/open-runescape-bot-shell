package org.openrsc.orbs.util.ocr.stormy.ocrlib;

interface JoiningCriterionT {

    boolean joinCritFunc(Region r1, Region r2);

}
