package org.openrsc.orbs.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.model.ItemDef;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@Component
public class EntityService {
    private static final Map<Integer, ItemDef> itemDefs = new HashMap<>();

    public EntityService(ResourceLoader resourceLoader) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<List<ItemDef>> ref = new TypeReference<>() {
            };
            List<ItemDef> defs = mapper.readValue(
                    resourceLoader.getResource("classpath:ItemDefs.json").getInputStream(),
                    ref
            );
            defs.forEach(def -> itemDefs.put(def.getId(), def));
        } catch (Exception e) {
            log.error("Unable to load Item Definitions from ItemDefs.json");
        }
    }

    public static ItemDef getItemDef(Integer id) {
        return itemDefs.get(id);
    }
}
