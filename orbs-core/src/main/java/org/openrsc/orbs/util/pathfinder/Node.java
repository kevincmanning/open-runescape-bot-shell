package org.openrsc.orbs.util.pathfinder;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.model.TileTraversal;

@Builder
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Node {
    @EqualsAndHashCode.Include
    private final Point point;
    private final TileTraversal traversal;
    private final Integer gCost;
    private final Integer hCost;
    private final Node parent;

    public Integer fCost() {
        return gCost + hCost;
    }
}
