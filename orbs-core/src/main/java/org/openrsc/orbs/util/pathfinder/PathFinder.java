package org.openrsc.orbs.util.pathfinder;

import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.model.Direction;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.model.TileTraversal;
import org.openrsc.orbs.model.TileTraversalProvider;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
public class PathFinder {
    private static final int MAX_ITERATIONS_DEFAULT = 1200;
    private final TileTraversalProvider tileProvider;
    private final Point start;
    private final Point target;
    private final int maxIterations;

    private final Map<Point, Node> nodes = new HashMap<>();
    private final Queue<Node> openNodes = new PriorityQueue<>(Comparator.comparing(Node::fCost));
    private final Set<Point> closedNodes = new HashSet<>();
    private final Set<Point> endLocations = new HashSet<>();

    public PathFinder(TileTraversalProvider tileProvider, Point start, Point target) {
        this(tileProvider, start, target, MAX_ITERATIONS_DEFAULT);
    }

    public PathFinder(TileTraversalProvider tileProvider, Point start, Point target, int maxIterations) {
        this.tileProvider = tileProvider;
        this.target = target;
        this.start = start;
        this.maxIterations = maxIterations;
    }

    public Path findPath() {
        try {
            int iterations = 0;
            TileTraversal targetTile = tileProvider.apply(target);
            if (!targetTile.isTraversable()) {
                // Cannot get to this endpoint, let's find the nearest point we can walk to that has access here
                endLocations.addAll(
                        targetTile.getAccessibleDirections(tileProvider)
                                .stream()
                                .map(target::move)
                                .collect(Collectors.toSet())
                );
//                log.debug("Unable to reach point so listing alternatives: " + StringUtils.join(endLocations, ","));
                if (endLocations.isEmpty()) {
                    return null;
                }
            } else {
                endLocations.add(target);
            }
            Node startNode = Node.builder()
                    .point(start)
                    .traversal(tileProvider.apply(start))
                    .gCost(0)
                    .hCost(estimateHCost(start))
                    .build();
            openNodes.add(startNode);
            while (!openNodes.isEmpty() && iterations <= maxIterations) {
                Node currentNode = openNodes.poll();
                closedNodes.add(currentNode.getPoint());

                if (endLocations.contains(currentNode.getPoint())) {
                    return buildPath(currentNode);
                }

                List<Node> movableNeighbors = currentNode.getTraversal().getMovableDirections(tileProvider)
                        .stream()
                        .map(direction -> currentNode.getPoint().move(direction))
                        .filter(point -> !closedNodes.contains(point))
                        .map(point -> nodeFromPoint(point, currentNode))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                for (Node neighbor : movableNeighbors) {
                    Node existing = nodes.get(neighbor.getPoint());
                    if (existing == null || neighbor.fCost() < existing.fCost()) {
                        openNodes.remove(existing);
                        openNodes.add(neighbor);
                    }
                    nodes.put(neighbor.getPoint(), neighbor);
                }
                iterations++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Node nodeFromPoint(Point point, Node parent) {
        return Node.builder()
                .point(point)
                .parent(parent)
                .traversal(tileProvider.apply(point))
                .gCost(calculateGCost(parent.getGCost(), parent.getPoint(), point))
                .hCost(estimateHCost(point))
                .build();
    }

    private int estimateHCost(final Point p) {
        return endLocations.stream()
                .mapToInt(point -> point.distance(p) * 10)
                .min()
                .getAsInt();
    }

    private int calculateGCost(int previousGCost, Point p, Point p2) {
        Point offset = new Point(p.getX() - p2.getX(), p.getY() - p2.getY());
        if (Math.abs(offset.getX()) - Math.abs(offset.getY()) == 0) {
            return previousGCost + 10;
        }
        return previousGCost + 10;
    }

    private Path buildPath(Node endNode) {
        LinkedList<Point> points = new LinkedList<>();
        Point releasePoint = endNode.getPoint();
        points.add(endNode.getPoint());
        Node next = endNode;
        int directionChanges = 0;
        Direction lastDirection = null;
        while ((next = next.getParent()) != null) {
            Point nextPoint = next.getPoint();

            // Calculate release point
            Direction direction = Direction.byOffset(releasePoint.getOffset(nextPoint));
            if (direction != lastDirection) {
                lastDirection = direction;
                directionChanges++;
            }
            if (directionChanges <= 2 && nextPoint.distance(points.getLast()) <= 5) {
                releasePoint = nextPoint;
            }

            // Build normal path
            points.addFirst(nextPoint);
        }
//        log.info(points);
        return Path.builder().points(points).releasePoint(releasePoint).build();
    }

}
