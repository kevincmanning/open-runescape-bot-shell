package org.openrsc.orbs.scripts;

import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldNameConstants;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.connection.player.script.CommonIds;
import org.openrsc.orbs.net.connection.player.script.ItemIds;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.connection.player.script.model.BarType;
import org.openrsc.orbs.net.connection.player.script.params.EnumParam;
import org.openrsc.orbs.net.connection.player.script.params.InstructionsParam;
import org.openrsc.orbs.net.connection.player.script.params.NumberParam;
import org.openrsc.orbs.net.connection.player.script.params.ScriptParam;

import java.util.List;
import java.util.Map;

public class PyraSmelter extends Script<PyraSmelter.Params> {
    private final Point BANK = new Point(328, 552);
    private final Point FURNACE = new Point(310, 545);

    public PyraSmelter(ScriptAPI api) {
        super(api);
    }

    private boolean hasEnoughOre(ScriptAPI api, BarType barType) {
        int oreId = barType.getOreId();
        int coalId = barType.getCoalId();
        int coalCount = barType.getCoalCount();

        return api.getInventoryCount(oreId) >= 1 && api.getInventoryCount(coalId) >= coalCount;
    }

    @Override
    public void init(Params params) {
        super.init(params);
    }

    @Override
    public void run() {
        api.sleepIfFatigueAbove(getParams().getFatigueLimit());
        final BarType barType = getParams().getBarType();
        if (hasEnoughOre(api, barType)) {
            if (!api.getLocation().equals(FURNACE)) {
                api.setStatus("Returning to furnace");
                api.walkTo(FURNACE);
            }

            api.findNearestObject(CommonIds.OBJ_FURANCE).ifPresent(furnace -> {
                api.setStatus("Smelting");
                api.useItemOnObject(
                        api.findItemInInventory(barType.getOreId()).orElse(-1),
                        furnace
                );
            });
        } else {
            api.setStatus("Banking");
            api.walkTo(BANK);
            api.openBank();
            api.depositAllExcept(ItemIds.SLEEPING_BAG.id());
            int coalRequired = barType.getCoalCount();
            int totalBarCost = coalRequired + 1;
            int multiplier = 29 / totalBarCost;
            int coalToWithdraw = multiplier * coalRequired;
            if (api.getBankCount(barType.getOreId()) < multiplier
                    || api.getBankCount(barType.getCoalId()) < coalToWithdraw) {
                api.shutdown();
                return;
            }
            api.withdraw(barType.getOreId(), multiplier);
            api.withdraw(barType.getCoalId(), coalToWithdraw);
            api.closeBank();
        }
        api.pause(100);
    }

    @Override
    public void shutdown() {
        if (!api.isInBank()) {
            api.walkTo(BANK);
            api.openBank();
        }
        api.depositAllExcept(ItemIds.SLEEPING_BAG.id());
        api.closeBank();
        done();
    }

    @Override
    public Map<String, String> getMetrics() {
        return null;
    }

    @Override
    public Params getDefaultParameters() {
        return Params.builder().build();
    }

    @Override
    public List<ScriptParam> getOptions() {
        return Lists.newArrayList(
                InstructionsParam.builder()
                        .key("instructions")
                        .label("Start in West Falador")
                        .build(),
                NumberParam.builder()
                        .key(Params.Fields.fatigueLimit)
                        .label("Fatigue Limit")
                        .build(),
                EnumParam.<BarType>builder()
                        .values(BarType.values())
                        .key(Params.Fields.barType)
                        .label("Ores")
                        .build()
        );
    }

    @Builder
    @Getter
    @FieldNameConstants
    public static class Params {
        @Builder.Default
        final int fatigueLimit = 95;
        @Builder.Default
        final BarType barType = BarType.STEEL;
    }
}
