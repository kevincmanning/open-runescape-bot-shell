package org.openrsc.orbs.scripts;

import com.google.common.eventbus.Subscribe;
import lombok.extern.log4j.Log4j2;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.connection.player.script.event.TradeRequest;

import java.util.Collections;
import java.util.Map;

@Log4j2
public class TradeDebugger extends Script<String> {
    public TradeDebugger(ScriptAPI api) {
        super(api);
    }

    @Override
    public void init(String params) {
        super.init(params);
    }

    @Override
    public void run() {
        api.pause(200);
    }

    @Override
    public void shutdown() {
        done();
    }

    @Subscribe
    public void onTradeRequest(TradeRequest request) {
        api.sendTradeRequest(request.getPlayer().getPlayerId());
    }

    @Override
    public Map<String, String> getMetrics() {
        return Collections.emptyMap();
    }

    @Override
    public String getDefaultParameters() {
        return "test";
    }
}
