package org.openrsc.orbs.scripts;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldNameConstants;
import org.openrsc.orbs.model.Area;
import org.openrsc.orbs.model.Point;
import org.openrsc.orbs.net.connection.player.script.CommonIds;
import org.openrsc.orbs.net.connection.player.script.ItemIds;
import org.openrsc.orbs.net.connection.player.script.Script;
import org.openrsc.orbs.net.connection.player.script.ScriptAPI;
import org.openrsc.orbs.net.connection.player.script.model.OreType;
import org.openrsc.orbs.net.connection.player.script.params.BooleanParam;
import org.openrsc.orbs.net.connection.player.script.params.EnumParam;
import org.openrsc.orbs.net.connection.player.script.params.NumberParam;
import org.openrsc.orbs.net.connection.player.script.params.ScriptParam;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class PyraMiner extends Script<PyraMiner.Pyrameters> {
    private final Map<String, String> metrics = new ConcurrentHashMap<>();
    private Point startPoint;
    private Miner miner;
    private String miningStatus;

    public PyraMiner(ScriptAPI api) {
        super(api);
    }

    @Override
    public void init(Pyrameters params) {
        super.init(params);
        this.startPoint = api.getLocation();
        this.miningStatus = MessageFormat.format(
                "Mining {0}",
                params.getOres()
                        .stream()
                        .map(String::valueOf)
                        .collect(joining(", "))
        );

        if (params.getOres().contains(OreType.COAL)) {
            miner = new MiningGuildMiner();
        } else {
            miner = new VarrockEastMiner();
        }
    }

    @Override
    public void run() {
        miner.accept(api);
    }

    @Override
    public void shutdown() {
        api.setStatus("Shutting down");
        miner.walkToBank(api);
        api.openBank();
        api.depositAllExcept(ItemIds.SLEEPING_BAG.id());
        api.closeBank();
        done();
    }

    @Override
    public Map<String, String> getMetrics() {
        return Map.of();
    }

    @Override
    public Pyrameters getDefaultParameters() {
        return Pyrameters.builder().build();
    }

    @Override
    public List<ScriptParam> getOptions() {
        return Lists.newArrayList(
                NumberParam.builder()
                        .key(Pyrameters.Fields.fatigueLimit)
                        .label("Fatigue Limit")
                        .build(),
                EnumParam.<OreType>builder()
                        .values(OreType.values())
                        .key(Pyrameters.Fields.ores)
                        .label("Ores")
                        .multiple(true)
                        .build(),
                BooleanParam.builder()
                        .key(Pyrameters.Fields.bankingEnabled)
                        .label("Banking")
                        .build()
        );
    }

    @AllArgsConstructor
    @Getter
    private enum PickaxeSpawns {
        SE_FALADOR(new Point(319, 659)),
        BARB_VILLAGE(new Point(231, 508));
        private final Point location;
    }

    @Builder
    @Getter
    @FieldNameConstants
    public static class Pyrameters {
        @Builder.Default
        final int fatigueLimit = 95;
        @Builder.Default
        final List<OreType> ores = Lists.newArrayList(OreType.COAL);
        @Builder.Default
        final boolean bankingEnabled = true;
    }

    abstract class Miner {
        private List<Integer> rockIds;

        public void accept(ScriptAPI api) {
            initRocks();
            api.sleepIfFatigueAbove(getParams().getFatigueLimit());
            if (api.getInventoryCount(CommonIds.PICKAXES) < 1) {
                obtainPickaxe(api);
            } else if (api.getInventory().size() < 30 || !getParams().isBankingEnabled()) {
                if (getParams().isBankingEnabled() && !getMiningArea().contains(api.getLocation())) {
                    returnToMiningArea(api);
                }

                //Try to mine rocks
                api.setStatus(miningStatus);
                api.findNearestObject(rockIds).ifPresent(obj -> {
                    api.useObject(obj);
                    api.pause(2000);
                });
            } else {
                bank(api);
            }
            api.pause(300);
        }

        private void obtainPickaxe(ScriptAPI api) {
            api.setStatus("Obtaining Pickaxe");
            walkToBank(api);
            api.openBank();
            boolean found = false;
            for (Integer pickaxeId : CommonIds.PICKAXES) {
                if (api.getBankCount(pickaxeId) > 0) {
                    api.withdraw(pickaxeId, 1);
                    found = true;
                    break;
                }
            }
            api.closeBank();
            if (!found) {
                Point currentLocation = api.getLocation();
                final Point groundFloorMiningArea = getMiningArea().getA().groundFloor();
                List<PickaxeSpawns> sorted = Arrays.stream(PickaxeSpawns.values()).sorted(
                        Comparator.comparingInt(pickaxe -> {
                            final Point location = pickaxe.getLocation();
                            return location.distance(currentLocation)
                                    + location.distance(groundFloorMiningArea);
                        })
                ).collect(Collectors.toList());
                for (PickaxeSpawns pickaxeLoc : sorted) {
                    final Point pickaxe = pickaxeLoc.getLocation();
                    if (api.isReachable(pickaxe)) {
                        api.walkTo(pickaxe);
                        // TODO: Wait for it to exist and verify that we have taken it
                        api.takeItem(pickaxe, ItemIds.BRONZE_PICKAXE.id());
                        break;
                    }
                }
            }
        }

        private void initRocks() {
            if (rockIds == null) {
                rockIds = new ArrayList<>();
                getParams().getOres().forEach(ore -> {
                    for (int objectId : ore.getObjectIds()) {
                        rockIds.add(objectId);
                    }
                });
            }
        }

        protected abstract void walkToBank(ScriptAPI api);

        protected void bank(ScriptAPI api) {
            api.setStatus("Banking");
            walkToBank(api);
            while (api.getInventory().size() >= 30) {
                api.openBank();
                api.depositAllExcept(
                        ItemIds.SLEEPING_BAG.id(),
                        ItemIds.BRONZE_PICKAXE.id(),
                        ItemIds.IRON_PICKAXE.id(),
                        ItemIds.STEEL_PICKAXE.id(),
                        ItemIds.MITHRIL_PICKAXE.id(),
                        ItemIds.ADAMANTITE_PICKAXE.id(),
                        ItemIds.RUNE_PICKAXE.id()
                );
                api.closeBank();
            }
            returnToMiningArea(api);
        }

        protected void walkToMiningArea(ScriptAPI api) {
            Area miningArea = getMiningArea();
            final int aX = miningArea.getA().getX();
            final int bX = miningArea.getB().getX();
            final int aY = miningArea.getA().getY();
            final int bY = miningArea.getB().getY();
            Point midpoint = new Point(
                    aX + (bX - aX) / 2,
                    aY + (bY - aY) / 2
            );
            api.walkTo(midpoint);
        }

        protected void returnToMiningArea(ScriptAPI api) {
            api.setStatus("Returning to Mining Area");
            walkToMiningArea(api);
        }

        protected Area getMiningArea() {
            return Area.builder().a(startPoint).b(startPoint).build();
        }
    }

    class VarrockEastMiner extends Miner {
        private final Area VARROCK_EAST_MINING_AREA = Area.builder()
                .a(new Point(81, 542))
                .b(new Point(59, 552))
                .build();

        @Override
        protected void walkToBank(ScriptAPI api) {
            api.walkTo(102, 511);
        }

        @Override
        protected void walkToMiningArea(ScriptAPI api) {
            api.walkTo(75, 544);
        }

        @Override
        protected Area getMiningArea() {
            return VARROCK_EAST_MINING_AREA;
        }
    }

    class MiningGuildMiner extends Miner {
        private final Area MINING_GUILD_AREA = Area.builder()
                .a(new Point(280, 3382))
                .b(new Point(262, 3401))
                .build();

        @Override
        protected void walkToBank(ScriptAPI api) {
            // Climb ladder
            api.walkTo(274, 3398);
            api.repeatUntil(
                    () -> api.isVisible(274, 566),
                    () -> api.useObjectAt(274, 3398),
                    5000,
                    1000
            );
            api.walkTo(279, 562);
        }

        @Override
        protected void returnToMiningArea(ScriptAPI api) {
            api.setStatus("Returning to Mining Guild");
            api.walkTo(274, 566);
            api.repeatUntil(
                    () -> api.isVisible(274, 3398),
                    () -> api.useObjectAt(274, 566),
                    5000,
                    1000
            );
        }

        @Override
        protected Area getMiningArea() {
            return MINING_GUILD_AREA;
        }
    }
}
